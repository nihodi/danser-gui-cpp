# Linux installation instructions
__________________________________
## Dependencies
- Qt 5
- ffmpeg if you want to record videos
- sqlite3

## Installation
1. Download [Danser](https://github.com/wieku/danser-go/releases)
2. Extract it somewhere
3. Download danser_gui_cpp from the [releases](https://gitlab.com/nihodi/danser-gui-cpp/-/releases)
4. Extract danser_gui_cpp somewhere
5. Move all the files from danser_gui_cpp into the danser directory
6. Run `./danser -md5 0` to import all beatmaps. Note: A valid osu! installation has to be located at `~/.osu`!
7. `./danser_gui_cpp` to open the GUI

Look at the [usage guide](USAGE.md) if you're confused about anything.

Please open an issue if you face any problems!