# Installing on Windows
_______________________

1. Download Danser from [here](https://github.com/Wieku/danser-go/releases/download/0.6.9/danser-0.6.9-win.zip)
2. Extract the zip file somewhere. You should then have a folder that looks like this: ![](img/install_windows_danser_zip.png)
3. Download danser-gui-cpp from the [releases](https://gitlab.com/nihodi/danser-gui-cpp/-/releases) page. Click on `danser_gui_cpp-1.0.0.7z` under the `Other` section!
4. Extract this zip somewhere. That folder should look like this: ![](img/install_windows_gui_zip.png)
5. Move ALL the contents from the danser-gui-cpp folder to the Danser folder. You should end up with something like this: ![](img/install_windows_finished.png)
6. Double click on `SETUP.bat` for first time setup. **MAKE SURE TO READ WHAT IT SAYS**. The GUI should launch after the setup is complete. 

To launch the after the first time setup, double click on `danser_gui_cpp.exe` and the GUI should open! Just make sure that you have run `SETUP.bat` before this!

**If you wish to record videos, download [ffmpeg](https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-n5.0-latest-win64-gpl-5.0.zip) and put `ffmpeg.exe` inside the installation folder.**