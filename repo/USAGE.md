# How to use the GUI
____________________

This GUI is capable of a lot of things, so it's important to know how to use it.

In the top-left corner you can choose between wanting to launch Danser (watch a playback), and wanting to change the settings for Danser.

![](img/howto_1.png)

## Playback
In the Playback tab, there are more tabs! Cursordance, Replay and the Knockout tab.

![](img/howto_2.png)

All of these tabs have this at the bottom and a button to launch Danser.
![](img/howto_4.png)
Here you can do a lot of things that have to do with playback. They should be pretty self-explanatory.

### The Cursordance tab

At the top you have this beatmap selector. Here you can search for beatmaps and select them from the list below.
After you have selected a map from the list, you can tweak the playback settings, and finally launch Danser by hitting the big
`Launch Danser!` button at the bottom!

**If some maps don't show up, try to press the `Reload songs from disk` button.**
![](img/howto_3.png)

### The Replay tab

This tab lets you watch replays in a fancy way! To watch a replay, click the `Browse` button and select a valid `.osr` file.
Then click the `Launch Danser!` button to see the replay!
![](img/howto_5.png)

### The Knockout tab

Knockouts are a way to view many replays at once.

First you need to place the replays you want to view into a specific folder. To do that, press the `Add replay files here! Click to open the folder!` button and add all the replays into this folder.

Then you need to select the beatmap in the same way as in the Cursordance tab.

Then you can tweak playback settings, and finally, launch Danser!

**Note: Your replays are saved each time. There is no need to copy them into the folder many times!**

## Editing Settings

To edit settings, all you have to do is go to the settings tab and change whatever you want.
Just remember to **save** the settings after you've changed something by clicking the `Save Settings!` button at the bottom.

If you don't know what a setting does, hover your mouse over it and a little tooltip will appear.
If that tooltip is confusing or too short, you can press `Shift+F1` to get a more detailed explanation, again, while hovering your mouse over the relevant setting.

Availible settings to change are:
- General - The location of your osu! folders and Discord rich presence.
- Graphics - Window size, VSync, Fullscreen, FPS Cap etc.
- Audio - Volumes + offset and toggles for beatmap samples.
- Cursor - Settings for the special Danser cursor. You can disable the Danser cursor by going into the Skin tab.
- Skin - Change skin and determine if the skin's cursor should be used, instead of Danser's special cursor.
- Knockout - Knockout settings.
- 
- 