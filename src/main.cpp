#include <QApplication>

#include "MainWindow.h"
#include "io/BeatmapManager.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	BeatmapManager::Init();

	MainWindow window;
	window.show();

	return QApplication::exec();
}
