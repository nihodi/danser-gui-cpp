#include "AboutAndHelpWidget.h"

#include <QDesktopServices>
#include <QUrl>

AboutAndHelpWidget::AboutAndHelpWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout(this);

	m_GoToRepo = new QPushButton("Open repository in browser");
	m_ReportBug = new QPushButton("Report a bug");
	m_GoToUsageTutorial = new QPushButton("Go to usage tutorial");
	m_GoToDanserWiki = new QPushButton("Go to the Danser wiki for in-depth explanation of settings");

	m_Layout->addWidget(new QLabel("A simple GUI for Danser written in C++ using Qt"), 0, 0, Qt::AlignCenter);
	m_Layout->addWidget(m_GoToRepo, 1, 0);
	m_Layout->addWidget(m_ReportBug, 2, 0);
	m_Layout->addWidget(m_GoToUsageTutorial, 3, 0);
	m_Layout->addWidget(m_GoToDanserWiki, 4, 0);

	connect(m_GoToRepo, SIGNAL(pressed()), this, SLOT(GoToRepo()));
	connect(m_ReportBug, SIGNAL(pressed()), this, SLOT(ReportBug()));
	connect(m_GoToUsageTutorial, SIGNAL(pressed()), this, SLOT(GoToUsageTutorial()));
	connect(m_GoToDanserWiki, SIGNAL(pressed()), this, SLOT(GoToDanserWiki()));

	setMaximumHeight(200);
}

void AboutAndHelpWidget::GoToRepo()
{
	QDesktopServices::openUrl(QUrl("https://gitlab.com/nihodi/danser-gui-cpp"));
}

void AboutAndHelpWidget::GoToUsageTutorial()
{
	QDesktopServices::openUrl(QUrl("https://gitlab.com/nihodi/danser-gui-cpp/-/blob/main/repo/USAGE.md"));
}

void AboutAndHelpWidget::ReportBug()
{
	QDesktopServices::openUrl(QUrl("https://gitlab.com/nihodi/danser-gui-cpp/-/issues"));
}

void AboutAndHelpWidget::GoToDanserWiki()
{
	QDesktopServices::openUrl(QUrl("https://github.com/Wieku/danser-go/wiki"));
}
