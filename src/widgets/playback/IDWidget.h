#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>

#include <QLabel>
#include <QLineEdit>

#include <QPushButton>

#include "CommonWidget.h"

class IDWidget : public QWidget
{
	Q_OBJECT

public:
	explicit IDWidget(QWidget* parent = nullptr);
	void LaunchDanser();

private:
	QGridLayout* m_Layout;

	QLabel* m_IDLabel;
	QLineEdit* m_IDInput;

	QGroupBox* m_GroupBox;
	QGridLayout* m_GroupLayout;

	CommonWidget* m_CommonWidget;
};
