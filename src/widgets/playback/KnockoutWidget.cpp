#include "KnockoutWidget.h"

#include "platform.h"

#include <iostream>
#include <filesystem>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>

KnockoutWidget::KnockoutWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout(this);

	m_OpenReplayFolder = new QPushButton("Add replay files here! Click to open the folder.");
	connect(m_OpenReplayFolder, SIGNAL(pressed()), this, SLOT(OpenReplayFolder()));

	m_BeatmapSelectorWidget = new BeatmapSelectorWidget;
	m_CommonWidget = new CommonWidget;

	m_Layout->addWidget(m_BeatmapSelectorWidget, 0, 0);
	m_Layout->addWidget(m_CommonWidget, 1, 0, 3, 1);
	m_Layout->addWidget(m_OpenReplayFolder, 4, 0);
}

void KnockoutWidget::LaunchDanser()
{
	std::cout << "Launch Danser pressed" << std::endl;

	// should be "danser" on linux and "danser.exe" on windows
	std::string command = DANSER;

	command += m_BeatmapSelectorWidget->GetCommand();

	if (command != DANSER)
	{
		command += m_CommonWidget->GetCommand();
		command += " -settings=danser-gui-cpp -knockout";

		std::cout << "Executing Danser with command: " << command << std::endl;

		system(command.c_str());
	}
	else 	// warn the user and do nothing if no beatmap is selected
	{
		QMessageBox::warning(this, "No beatmap selected!", "You have not selected a beatmap!");
	}
}

void KnockoutWidget::OpenReplayFolder()
{
	QUrl folder;

#ifdef linux

	QString path(HOME);
	path += "/.local/share/danser/replays";
    std::filesystem::create_directory(path.toStdString());

	folder.setPath(path);
	QDesktopServices::openUrl(folder);

#else
    std::filesystem::create_directory("replays");
    system("start replays");
#endif
}
