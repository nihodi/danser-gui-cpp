#include "CommonWidget.h"

CommonWidget::CommonWidget(QWidget* parent) : QWidget(parent)
{

	m_Layout = new QVBoxLayout(this);

	m_PlaybackLayout = new QGridLayout;
	m_PlaybackGroupBox = new QGroupBox("Playback settings");
	m_PlaybackGroupBox->setLayout(m_PlaybackLayout);

	CreateGeneralGroupBox();
	CreateStartEndGroupBox();
	CreateMetadataGroupBox();
	CreateModGroupBox();

	m_PlaybackLayout->addWidget(m_GeneralGroupBox, 0, 0);
	m_PlaybackLayout->addWidget(m_ModGroupBox, 0, 1);
	m_PlaybackLayout->addWidget(m_StartEndGroupBox, 1, 0);
	m_PlaybackLayout->addWidget(m_MetadataGroupBox, 1, 1);


	m_Layout->addWidget(m_PlaybackGroupBox);
	setMaximumHeight(400);
}

std::string CommonWidget::GetCommand()
{
	std::string command;

	if (m_SkipInput->isChecked())
		command += " -skip";
	if (m_RecordInput->isChecked())
		command += " -record";

	if (m_SpeedInput->value() != 1.0 && m_SpeedInput->isEnabled())
		command += " -speed=" + std::to_string(m_SpeedInput->value());
	if (m_PitchInput->value() != 1.0 && m_PitchInput->isEnabled())
		command += " -pitch=" + std::to_string(m_PitchInput->value());

	if (m_ModGroupBox->isChecked())
	{
		command += " -mods=\"";
		if (m_EZMod->isChecked())
			command += "EZ";
		if (m_HTMod->isChecked())
			command += "HT";
		if (m_HRMod->isChecked())
			command += "HR";
		if (m_DTMod->isChecked())
			command += "DT";
		if (m_NCMod->isChecked())
			command += "NC";
		if (m_HDMod->isChecked())
			command += "HD";
		if (m_FLMod->isChecked())
			command +=" FL";

		command +="\"";
	}

	// AR/CS override
	if (m_AROverride->isChecked() && m_AROverride->isEnabled())
		command += " -ar=" + std::to_string(m_ARInput->value());
	if (m_CSOverride->isChecked() && m_CSOverride->isEnabled())
		command += " -cs=" + std::to_string(m_CSInput->value());

	// START/END override
	if (m_StartCheckBox->isChecked())
		command += " -start=" + std::to_string(m_StartInput->value());
	if (m_EndCheckBox->isChecked())
		command += " -end=" + std::to_string(m_EndInput->value());

	return command;
}

void CommonWidget::CreateGeneralGroupBox()
{
	m_GeneralGroupBox = new QGroupBox("General");
	m_GeneralLayout = new QGridLayout;
	m_GeneralGroupBox->setLayout(m_GeneralLayout);

	m_SkipInput = new QCheckBox;
	m_RecordInput = new QCheckBox;

	m_SpeedInput = new QDoubleSpinBox;
	m_SpeedInput->setRange(0.05, 2.0);
	m_SpeedInput->setSingleStep(0.05);
	m_SpeedInput->setSuffix("x");
	m_SpeedInput->setValue(1.0);
	m_SpeedInput->setDecimals(2);

	m_PitchInput = new QDoubleSpinBox;
	m_PitchInput->setRange(0.05, 2.0);
	m_PitchInput->setSingleStep(0.05);
	m_PitchInput->setSuffix("x");
	m_PitchInput->setValue(1.0);
	m_PitchInput->setDecimals(2);

	m_GeneralLayout->addWidget(new QLabel("Skip intro of the map"), 0, 0);
	m_GeneralLayout->addWidget(new QLabel("Record"), 1, 0);
	m_GeneralLayout->addWidget(new QLabel("Speed multiplier"), 2, 0);
	m_GeneralLayout->addWidget(new QLabel("Pitch multiplier"), 3, 0);
	m_GeneralLayout->addWidget(m_SkipInput, 0, 1);
	m_GeneralLayout->addWidget(m_RecordInput, 1, 1);
	m_GeneralLayout->addWidget(m_SpeedInput, 2, 1);
	m_GeneralLayout->addWidget(m_PitchInput, 3, 1);
}

void CommonWidget::CreateStartEndGroupBox()
{
// =============== START/END OVERRIDE STUFF ===============

	m_StartEndLayout = new QGridLayout;
	m_StartEndGroupBox = new QGroupBox("Override start/end point of playback");

	m_StartCheckBox = new QCheckBox;
	m_EndCheckBox = new QCheckBox;

	m_StartInput = new QDoubleSpinBox;
	m_StartInput->setValue(0.0);
	m_StartInput->setRange(0.0, 1000000.0);
	m_StartInput->setSingleStep(0.5);
	m_StartInput->setSuffix(" secs");
	m_StartInput->setDisabled(true);
	m_StartInput->setDecimals(2);

	m_EndInput = new QDoubleSpinBox;
	m_EndInput->setValue(0.0);
	m_EndInput->setRange(0.0, 1000000.0);
	m_EndInput->setSingleStep(0.5);
	m_EndInput->setSuffix(" secs");
	m_EndInput->setDisabled(true);
	m_EndInput->setDecimals(2);

	m_StartEndLayout->addWidget(new QLabel("Override start point"), 0, 0);
	m_StartEndLayout->addWidget(new QLabel("Override end point"), 1, 0);
	m_StartEndLayout->addWidget(m_StartCheckBox, 0, 1);
	m_StartEndLayout->addWidget(m_StartInput, 0, 2);
	m_StartEndLayout->addWidget(m_EndCheckBox, 1, 1);
	m_StartEndLayout->addWidget(m_EndInput, 1, 2);

	connect(m_StartCheckBox, SIGNAL(clicked(bool)), m_StartInput, SLOT(setEnabled(bool)));
	connect(m_EndCheckBox, SIGNAL(clicked(bool)), m_EndInput, SLOT(setEnabled(bool)));

	m_StartEndGroupBox->setLayout(m_StartEndLayout);

}

void CommonWidget::CreateMetadataGroupBox()
{
	// ================= AR/CS OVERRIDE STUFF =================

	m_MetadataLayout = new QGridLayout;

	m_MetadataGroupBox = new QGroupBox("Override AR/CS values");

	// AR OVERRIDE
	m_AROverride = new QCheckBox;
	m_AROverride->setChecked(false);

	m_ARInput = new QDoubleSpinBox;
	m_ARInput->setRange(0.0, 12.0);
	m_ARInput->setSingleStep(0.1);
	m_ARInput->setEnabled(false);
	m_ARInput->setPrefix("AR ");
	m_ARInput->setDecimals(1);

	// CS OVERRIDE
	m_CSOverride = new QCheckBox;
	m_CSOverride->setChecked(false);

	m_CSInput = new QDoubleSpinBox;
	m_CSInput->setRange(0.0, 12.0);
	m_CSInput->setSingleStep(0.1);
	m_CSInput->setEnabled(false);
	m_CSInput->setPrefix("CS ");
	m_CSInput->setDecimals(1);

	// adding stuff to the GroupBox

	m_MetadataLayout->addWidget(new QLabel("Override AR"), 0, 0);
	m_MetadataLayout->addWidget(m_AROverride, 0, 1);
	m_MetadataLayout->addWidget(m_ARInput, 0, 2);

	m_MetadataLayout->addWidget(new QLabel("Override CS"), 1, 0);
	m_MetadataLayout->addWidget(m_CSOverride, 1, 1);
	m_MetadataLayout->addWidget(m_CSInput, 1, 2);


	connect(m_AROverride, SIGNAL(clicked(bool)), m_ARInput, SLOT(setEnabled(bool)));
	connect(m_CSOverride, SIGNAL(clicked(bool)), m_CSInput, SLOT(setEnabled(bool)));

	m_MetadataGroupBox->setLayout(m_MetadataLayout);
}

void CommonWidget::CreateModGroupBox()
{
	m_ModLayout = new QGridLayout;

	m_ModGroupBox = new QGroupBox("Mod override");
	m_ModGroupBox->setCheckable(true);
	m_ModGroupBox->setChecked(false);
	m_ModGroupBox->setLayout(m_ModLayout);

	m_EZMod = new QCheckBox;
	m_HTMod = new QCheckBox;
	m_HRMod = new QCheckBox;
	m_DTMod = new QCheckBox;
	m_NCMod = new QCheckBox;
	m_HDMod = new QCheckBox;
	m_FLMod = new QCheckBox;

	m_ModLayout->addWidget(new QLabel("EZ"), 0, 3, Qt::AlignRight);
	m_ModLayout->addWidget(new QLabel("HT"), 0, 5, Qt::AlignRight);
	m_ModLayout->addWidget(new QLabel("HR"), 1, 0, Qt::AlignRight);
	m_ModLayout->addWidget(new QLabel("DT"), 1, 2, Qt::AlignRight);
	m_ModLayout->addWidget(new QLabel("NC"), 1, 4, Qt::AlignRight);
	m_ModLayout->addWidget(new QLabel("HD"), 1, 6, Qt::AlignRight);
	m_ModLayout->addWidget(new QLabel("FL"), 1, 8, Qt::AlignRight);

	m_ModLayout->addWidget(m_EZMod, 0, 4, Qt::AlignHCenter);
	m_ModLayout->addWidget(m_HTMod, 0, 6, Qt::AlignHCenter);
	m_ModLayout->addWidget(m_HRMod, 1, 1, Qt::AlignHCenter);
	m_ModLayout->addWidget(m_DTMod, 1, 3, Qt::AlignHCenter);
	m_ModLayout->addWidget(m_NCMod, 1, 5, Qt::AlignHCenter);
	m_ModLayout->addWidget(m_HDMod, 1, 7, Qt::AlignHCenter);
	m_ModLayout->addWidget(m_FLMod, 1, 9, Qt::AlignHCenter);

	// disable conflicting mods
	connect(m_EZMod, SIGNAL(clicked(bool)), m_HRMod, SLOT(setDisabled(bool)));
	connect(m_HTMod, SIGNAL(clicked(bool)), m_DTMod, SLOT(setDisabled(bool)));
	connect(m_HRMod, SIGNAL(clicked(bool)), m_EZMod, SLOT(setDisabled(bool)));
	connect(m_DTMod, SIGNAL(clicked(bool)), m_HTMod, SLOT(setDisabled(bool)));
	connect(m_DTMod, SIGNAL(clicked(bool)), m_NCMod, SLOT(setDisabled(bool)));
	connect(m_NCMod, SIGNAL(clicked(bool)), m_DTMod, SLOT(setDisabled(bool)));

	// disable other things
	connect(m_DTMod, SIGNAL(clicked(bool)), m_SpeedInput, SLOT(setDisabled(bool)));
	connect(m_HTMod, SIGNAL(clicked(bool)), m_SpeedInput, SLOT(setDisabled(bool)));
	connect(m_NCMod, SIGNAL(clicked(bool)), m_SpeedInput, SLOT(setDisabled(bool)));
	connect(m_NCMod, SIGNAL(clicked(bool)), m_PitchInput, SLOT(setDisabled(bool)));

	connect(m_HRMod, SIGNAL(clicked(bool)), this, SLOT(HR_EZClicked(bool)));
	connect(m_EZMod, SIGNAL(clicked(bool)), this, SLOT(HR_EZClicked(bool)));

	// ModGroupBox disabled / enabled
	connect(m_ModGroupBox, SIGNAL(clicked(bool)), this, SLOT(HR_EZClicked(bool)));
	connect(m_ModGroupBox, SIGNAL(clicked(bool)), this, SLOT(ModGroupBoxClicked(bool)));
}

void CommonWidget::HR_EZClicked(bool enabled)
{
	if (enabled)
	{
		m_CSInput->setDisabled(true);
		m_CSOverride->setDisabled(true);
		m_ARInput->setDisabled(true);
		m_AROverride->setDisabled(true);
	}
	else
	{
		m_CSInput->setEnabled(m_CSOverride->isChecked());
		m_ARInput->setEnabled(m_AROverride->isChecked());

		m_CSOverride->setEnabled(true);
		m_AROverride->setEnabled(true);
	}
}

void CommonWidget::ModGroupBoxClicked(bool enabled)
{

	if (m_DTMod->isChecked() || m_NCMod->isChecked() || m_HTMod->isChecked())
		m_SpeedInput->setDisabled(enabled);
	if (m_NCMod->isChecked())
		m_PitchInput->setDisabled(enabled);

}
