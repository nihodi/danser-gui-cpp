#pragma once

#include <QWidget>
#include <QGridLayout>

#include <QPushButton>

#include "BeatmapSelectorWidget.h"
#include "CommonWidget.h"

class KnockoutWidget : public QWidget
{
	Q_OBJECT

public:
	explicit KnockoutWidget(QWidget* parent = nullptr);

	void LaunchDanser();

private slots:
	void OpenReplayFolder();

private:
	QGridLayout* m_Layout;

	QPushButton* m_OpenReplayFolder;

	BeatmapSelectorWidget* m_BeatmapSelectorWidget;
	CommonWidget* m_CommonWidget;
};
