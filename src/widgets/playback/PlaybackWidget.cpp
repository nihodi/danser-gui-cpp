#include "PlaybackWidget.h"

#include <iostream>
#include <QScrollArea>

PlaybackWidget::PlaybackWidget(QWidget* parent)
	: QWidget(parent)
{
	m_TabWidget = new QTabWidget;
	m_Layout = new QGridLayout(this);

	m_LaunchDanserButton = new QPushButton("Launch Danser!");

	m_DanserWidget = new DanserWidget;
	m_ReplayWidget = new ReplayWidget;
	m_IDWidget = new IDWidget;
	m_KnockoutWidget = new KnockoutWidget;

	m_TabWidget->addTab(m_DanserWidget, "Cursordance");
	m_TabWidget->addTab(m_ReplayWidget, "Replay");
	//m_TabWidget->addTab(m_IDWidget, "Beatmap ID");
	m_TabWidget->addTab(m_KnockoutWidget, "Knockout");

	m_Layout->addWidget(m_TabWidget, 0, 0);
	m_Layout->addWidget(m_LaunchDanserButton, 1, 0);

	connect(m_LaunchDanserButton, SIGNAL(pressed()), this, SLOT(LaunchDanserPressed()));
}

void PlaybackWidget::LaunchDanserPressed()
{
	switch (m_TabWidget->currentIndex())
	{
		case 0: // DanserWidget
			m_DanserWidget->LaunchDanser();
			break;

		case 1:
			m_ReplayWidget->LaunchDanser();
			break;

		case 2:
			m_KnockoutWidget->LaunchDanser();
			break;

		default:
			std::cout << "PlaybackWidget::LaunchDanserPressed index out of bounds!" << std::endl;
			break;

	}
}
