#include "DanserWidget.h"
#include "platform.h"

#include <iostream>
#include <QMessageBox>

DanserWidget::DanserWidget(QWidget* parent)
{
	m_Layout = new QGridLayout(this);

	m_BeatmapSelectorWidget = new BeatmapSelectorWidget;
	m_CommonWidget = new CommonWidget;

	m_Layout->addWidget(m_BeatmapSelectorWidget, 0, 0, 1, 1);
	m_Layout->addWidget(m_CommonWidget, 1, 0, 4, 1);

	setLayout(m_Layout);
}

void DanserWidget::LaunchDanser()
{
	std::cout << "Launch Danser pressed" << std::endl;

	// should be "danser" on linux and "danser.exe" on windows
	std::string command = DANSER;

	command += m_BeatmapSelectorWidget->GetCommand();

	if (command != DANSER)
	{
		command += m_CommonWidget->GetCommand();
		command += " -settings=danser-gui-cpp";

		std::cout << "Executing Danser with command: " << command << std::endl;

		system(command.c_str());
	}
	else 	// warn the user and do nothing if no beatmap is selected
	{
		QMessageBox::warning(this, "No beatmap selected!", "You have not selected a beatmap!");
	}

}
