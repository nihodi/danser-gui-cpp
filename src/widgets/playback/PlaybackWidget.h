#pragma once


#include <QWidget>
#include <QTabWidget>
#include <QGridLayout>

#include <QPushButton>

#include "DanserWidget.h"
#include "ReplayWidget.h"
#include "IDWidget.h"
#include "KnockoutWidget.h"

class PlaybackWidget : public QWidget
{
	Q_OBJECT

public:
	explicit PlaybackWidget(QWidget* parent = nullptr);

private slots:
	void LaunchDanserPressed();

private:
	QTabWidget* m_TabWidget;
	QGridLayout* m_Layout;

	QPushButton* m_LaunchDanserButton;

	DanserWidget* m_DanserWidget;
	ReplayWidget* m_ReplayWidget;
	IDWidget* m_IDWidget;
	KnockoutWidget* m_KnockoutWidget;
};
