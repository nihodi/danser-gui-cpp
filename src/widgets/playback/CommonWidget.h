#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QGroupBox>

#include <QLabel>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QSlider>

class CommonWidget : public QWidget
{
Q_OBJECT

public:
	explicit CommonWidget(QWidget* parent = nullptr);

	std::string GetCommand();

private slots:
	void HR_EZClicked(bool enabled);
	void ModGroupBoxClicked(bool enabled);

private:
	void CreateGeneralGroupBox();
	void CreateStartEndGroupBox();
	void CreateMetadataGroupBox();
	void CreateModGroupBox();

	// stuff for playback settings
	QGridLayout* m_PlaybackLayout;
	QGroupBox* m_PlaybackGroupBox;

	QVBoxLayout* m_Layout;

	// general
	QGridLayout* m_GeneralLayout;
	QGroupBox* m_GeneralGroupBox;

	QCheckBox* m_SkipInput;
	QCheckBox* m_RecordInput;
	QDoubleSpinBox* m_SpeedInput;
	QDoubleSpinBox* m_PitchInput;

	// mod override
	QGridLayout* m_ModLayout;
	QGroupBox* m_ModGroupBox;

	QCheckBox* m_EZMod;
	QCheckBox* m_HTMod;
	QCheckBox* m_HRMod;
	QCheckBox* m_DTMod;
	QCheckBox* m_NCMod;
	QCheckBox* m_HDMod;
	QCheckBox* m_FLMod;

	// Start/end override part
	QGridLayout* m_StartEndLayout;
	QGroupBox* m_StartEndGroupBox;

	QDoubleSpinBox* m_StartInput;
	QDoubleSpinBox* m_EndInput;

	QCheckBox* m_StartCheckBox;
	QCheckBox* m_EndCheckBox;

	// stuff for metadata settings
	QGridLayout* m_MetadataLayout;
	QGroupBox* m_MetadataGroupBox;

	QCheckBox* m_AROverride;
	QDoubleSpinBox* m_ARInput;

	QCheckBox* m_CSOverride;
	QDoubleSpinBox* m_CSInput;
};
