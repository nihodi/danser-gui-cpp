#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "CommonWidget.h"
#include "BeatmapSelectorWidget.h"

class DanserWidget : public QWidget
{
	Q_OBJECT

public:
	explicit DanserWidget(QWidget* parent = nullptr);
	void LaunchDanser();

private:
	QGridLayout* m_Layout;

	CommonWidget* m_CommonWidget;
	BeatmapSelectorWidget* m_BeatmapSelectorWidget;
};
