#include "IDWidget.h"
#include "platform.h"

#include <iostream>
#include <QMessageBox>

IDWidget::IDWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout;

	m_GroupBox = new QGroupBox("Beatmap ID");
	m_GroupLayout = new QGridLayout;

	m_IDLabel = new QLabel("Beatmap ID");
	m_IDInput = new QLineEdit;
	m_IDInput->setPlaceholderText("ID");
	m_IDInput->setValidator( new QIntValidator(0, 2147483647, this));

	m_GroupLayout->addWidget(m_IDLabel, 0, 0);
	m_GroupLayout->addWidget(m_IDInput, 0, 1);
	m_GroupBox->setLayout(m_GroupLayout);

	m_CommonWidget = new CommonWidget;

	m_Layout->addWidget(m_GroupBox, 0, 0);
	m_Layout->addWidget(m_CommonWidget, 1, 0);

	setLayout(m_Layout);
	setMaximumHeight(450);
}

void IDWidget::LaunchDanser()
{
	std::string command = DANSER;

	if (m_IDInput->text() != "")
	{
		command += " -id=" + m_IDInput->text().toStdString();
		command += m_CommonWidget->GetCommand();

		std::cout << "Executing Danser with command: " << command << std::endl;
		system(command.c_str());
	}
	else
	{
		QMessageBox::warning(this, "No ID inputted", "You have not inputted any Beatmap ID!");
	}
}
