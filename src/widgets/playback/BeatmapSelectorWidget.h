#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>

#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>

#include "dialogues/BeatmapSelectDialog.h"

class BeatmapSelectorWidget : public QWidget
{
	Q_OBJECT

public:
	explicit BeatmapSelectorWidget(QWidget* parent = nullptr);

	std::string GetCommand();

private slots:
	void BeatmapSelectButtonPressed();

private:
	QGridLayout* m_Layout;

	QGridLayout* m_GroupLayout;
	QGroupBox* m_GroupBox;

	BeatmapSelectDialog m_BeatmapSelectDialog = BeatmapSelectDialog(this);
	QPushButton* m_SelectBeatmapButton;
	QLabel* m_CurrentBeatmap;
};
