#include "ReplayWidget.h"
#include "platform.h"

#include <iostream>
#include <QMessageBox>

ReplayWidget::ReplayWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout(this);

	m_GroupBox = new QGroupBox("Replay file");
	m_GroupLayout = new QGridLayout;

	m_InfoLabel = new QLabel;
	m_InfoLabel->setText("Select a replay file (.osr file) below");
	//m_InfoLabel->setMaximumHeight(10);


	m_LineEdit = new QLineEdit;
	m_LineEdit->setPlaceholderText(".osr file");
	m_LineEdit->setToolTip("Choose a file using the Browse button");

	m_BrowseButton = new QPushButton;
	m_BrowseButton->setText("Browse");

	m_CommonWidget = new CommonWidget;

	m_GroupLayout->addWidget(m_InfoLabel, 0, 0, 1, 2);
	m_GroupLayout->addWidget(m_LineEdit, 1, 0);
	m_GroupLayout->addWidget(m_BrowseButton, 1, 1);

	m_GroupBox->setLayout(m_GroupLayout);
	m_GroupBox->setMaximumHeight(100);

	m_Layout->addWidget(m_GroupBox, 0, 0);
	m_Layout->addWidget(m_CommonWidget, 1, 0);

	connect(m_BrowseButton, SIGNAL(pressed()), this, SLOT(BrowseButtonPressed()));

	setLayout(m_Layout);
	setMaximumHeight(m_GroupBox->maximumHeight() + m_CommonWidget->maximumHeight() + 50);
}

void ReplayWidget::BrowseButtonPressed()
{
	QString file = QFileDialog::getOpenFileName(this, "Open Replay", HOME, "osu! Replay (*.osr)");

	if (file.isEmpty())
		return;
	m_LineEdit->setText(file);
}

void ReplayWidget::LaunchDanser()
{
	std::string command = DANSER;

	if (m_LineEdit->text() != "")
	{
		command += " -r \"" + m_LineEdit->text().toStdString() + "\"";
		command += m_CommonWidget->GetCommand();

		std::cout << "Executing Danser with command: " << command << std::endl;

		system(command.c_str());
	}
	else
	{
		QMessageBox::warning(this, "No file selected", "You have not selected a file!");
	}


}
