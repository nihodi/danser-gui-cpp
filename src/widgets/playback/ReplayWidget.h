#pragma once


#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>

#include <QLineEdit>
#include <QFileDialog>
#include <QPushButton>
#include <QLabel>

#include "CommonWidget.h"

class ReplayWidget : public QWidget
{
	Q_OBJECT

public:
	explicit ReplayWidget(QWidget* parent = nullptr);
	void LaunchDanser();

private slots:
	void BrowseButtonPressed();


private:
	QGridLayout* m_Layout;

	QGroupBox* m_GroupBox;
	QGridLayout* m_GroupLayout;

	QLabel* m_InfoLabel;

	QLineEdit* m_LineEdit;
	QPushButton* m_BrowseButton;

	CommonWidget* m_CommonWidget;

};
