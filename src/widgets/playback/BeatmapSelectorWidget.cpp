#include "BeatmapSelectorWidget.h"

#include "io/BeatmapManager.h"
#include "beatmap/BeatmapListWidgetItem.h"

BeatmapSelectorWidget::BeatmapSelectorWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout;

	m_GroupLayout = new QGridLayout;
	m_GroupBox = new QGroupBox;

	m_SelectBeatmapButton = new QPushButton("Select beatmap");
	m_CurrentBeatmap = new QLabel("No beatmap currently selected!");

	m_GroupLayout->addWidget(m_SelectBeatmapButton, 0, 0);
	m_GroupLayout->addWidget(m_CurrentBeatmap, 1, 0, Qt::AlignHCenter);

	m_GroupBox->setLayout(m_GroupLayout);

	m_Layout->addWidget(m_GroupBox);
	setLayout(m_Layout);

	connect(m_SelectBeatmapButton, SIGNAL(pressed()), this, SLOT(BeatmapSelectButtonPressed()));
}

std::string BeatmapSelectorWidget::GetCommand()
{
	std::string command;

	command = m_BeatmapSelectDialog.GetCommand();

	return command;
}

void BeatmapSelectorWidget::BeatmapSelectButtonPressed()
{
	m_BeatmapSelectDialog.exec();

	// update text
	QString text;
	if (m_BeatmapSelectDialog.IsBeatmapSelected())
	{
		text = "Currently selected map: ";
		text += m_BeatmapSelectDialog.GetCurrentBeatmap().toString().c_str();
	}
	else
		text = "No beatmap selected!";

	m_CurrentBeatmap->setText(text);
}
