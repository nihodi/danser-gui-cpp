#pragma once

#include<QWidget>
#include <QGridLayout>

#include <QLabel>
#include <QPushButton>

class AboutAndHelpWidget : public QWidget
{
	Q_OBJECT

public:
	explicit AboutAndHelpWidget(QWidget* parent = nullptr);

private slots:
	void GoToRepo();
	void GoToUsageTutorial();
	void ReportBug();

	void GoToDanserWiki();

private:
	QGridLayout* m_Layout;

	QPushButton* m_GoToRepo;
	QPushButton* m_GoToUsageTutorial;
	QPushButton* m_ReportBug;

	QPushButton* m_GoToDanserWiki;
};
