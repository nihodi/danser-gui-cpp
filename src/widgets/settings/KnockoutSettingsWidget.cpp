#include "KnockoutSettingsWidget.h"

KnockoutSettingsWidget::KnockoutSettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout(this);

	CreateGroupBox();
	m_Layout->addWidget(m_GroupBox, 0, 0);

	setMaximumHeight(m_GroupBox->maximumHeight());
}

void KnockoutSettingsWidget::CreateGroupBox()
{
	m_GroupLayout = new QGridLayout;
	m_GroupBox = new QGroupBox;
	m_GroupBox->setLayout(m_GroupLayout);

	m_Mode = new QComboBox;
	m_Mode->addItems({"Breaking combo for the first time", "Breaking combo after reaching max combo",
					  "No-one gets knocked out", "No-one gets knocked out, but 200s and 50s will be shown",
					  "Loosing SS"});

	m_SortBy = new QComboBox;
	m_SortBy->addItems({"Score", "Accuracy", "PP"});

	m_GraceEndTime = new QDoubleSpinBox;
	m_GraceEndTime->setRange(-1000, 1000);
	m_GraceEndTime->setDecimals(2);
	m_GraceEndTime->setSingleStep(0.05);
	m_GraceEndTime->setSuffix(" secs");
	m_GraceEndTime->setToolTip("In \"Breaking combo for the first time\"-mode, players won't be knocked out if they break before this value.");

	m_BubbleMinimumCombo = new QSpinBox;
	m_BubbleMinimumCombo->setRange(0, 10000);
	m_BubbleMinimumCombo->setSingleStep(1);
	m_BubbleMinimumCombo->setSuffix(" combo");
	m_BubbleMinimumCombo->setToolTip("In modes where no-one gets knocked out, combo breaks won't be shown if the players combo is below this value.");

	m_MaxPlayers = new QSpinBox;
	m_MaxPlayers->setRange(0, 300);
	m_MaxPlayers->setSingleStep(1);

	m_RevivePlayersAtEnd = new QCheckBox;
	m_LiveSort = new QCheckBox;
	m_HideOverlayOnBreaks = new QCheckBox;

	m_AddDanser = new QCheckBox;
	m_AddDanser->setToolTip("Enable/Disable danser appearing in knockouts.");

	m_MinCursorSize = new QDoubleSpinBox;
	m_MinCursorSize->setRange(0, 100);
	m_MinCursorSize->setSingleStep(1);
	m_MinCursorSize->setToolTip("The size of the cursor when all players are alive.");

	m_MaxCursorSize = new QDoubleSpinBox;
	m_MaxCursorSize->setRange(0, 100);
	m_MaxCursorSize->setSingleStep(1);
	m_MaxCursorSize->setToolTip("The size of the cursor when only one player is alive.");

	m_DanserName = new QLineEdit;
	m_DanserName->setPlaceholderText("Danser's name on the leaderboard");
	m_DanserName->setToolTip("The name danser uses in knockouts.");

	auto graceEndTime = new QLabel("Grace end time");
	auto bubbleCombo = new QLabel("Only show combo breaks over");
	auto minSize = new QLabel("Cursor size when all players are alive");
	auto maxSize = new QLabel("Cursor size when only 1 player is alive");
	auto addDanser = new QLabel("Add Danser as a player");
	auto danserName = new QLabel("Danser's name on the leaderboard");

	graceEndTime->setToolTip(m_GraceEndTime->toolTip());
	bubbleCombo->setToolTip(m_BubbleMinimumCombo->toolTip());
	addDanser->setToolTip(m_AddDanser->toolTip());

	m_GroupLayout->addWidget(new QLabel("Players get knocked out when"), 0, 0);
	m_GroupLayout->addWidget(new QLabel("Leaderboard sort mode"), 1, 0);
	m_GroupLayout->addWidget(graceEndTime, 2, 0);
	m_GroupLayout->addWidget(bubbleCombo, 3, 0);
	m_GroupLayout->addWidget(new QLabel("Max players"), 4, 0);
	m_GroupLayout->addWidget(new QLabel("Revive all players at the end"), 5, 0);
	m_GroupLayout->addWidget(new QLabel("Sort leaderboard in real-time"), 6, 0);
	m_GroupLayout->addWidget(new QLabel("Hide the overlay on breaks"), 7, 0);
	m_GroupLayout->addWidget(minSize, 8, 0);
	m_GroupLayout->addWidget(maxSize, 9, 0);
	m_GroupLayout->addWidget(addDanser, 10, 0);
	m_GroupLayout->addWidget(danserName, 11, 0);

	m_GroupLayout->addWidget(m_Mode, 0, 1);
	m_GroupLayout->addWidget(m_SortBy, 1, 1);
	m_GroupLayout->addWidget(m_GraceEndTime, 2, 1);
	m_GroupLayout->addWidget(m_BubbleMinimumCombo, 3, 1);
	m_GroupLayout->addWidget(m_MaxPlayers, 4, 1);
	m_GroupLayout->addWidget(m_RevivePlayersAtEnd, 5, 1);
	m_GroupLayout->addWidget(m_LiveSort, 6, 1);
	m_GroupLayout->addWidget(m_HideOverlayOnBreaks, 7, 1);
	m_GroupLayout->addWidget(m_MinCursorSize, 8, 1);
	m_GroupLayout->addWidget(m_MaxCursorSize, 9, 1);
	m_GroupLayout->addWidget(m_AddDanser, 10, 1);
	m_GroupLayout->addWidget(m_DanserName, 11, 1);

	m_GroupBox->setMaximumHeight(500);
}


void KnockoutSettingsWidget::ReadSettings(const Settings& settings)
{
	m_Mode->setCurrentIndex(settings.Knockout.Mode);

	const QString& sortBy = settings.Knockout.SortBy;
	if (sortBy == "Score")
		m_SortBy->setCurrentIndex(0);
	else if (sortBy == "Accuracy")
		m_SortBy->setCurrentIndex(1);
	else
		m_SortBy->setCurrentIndex(2);

	m_GraceEndTime->setValue(settings.Knockout.GraceEndTime);
	m_BubbleMinimumCombo->setValue(settings.Knockout.BubbleMinimumCombo);
	m_MaxPlayers->setValue(settings.Knockout.MaxPlayers);
	m_RevivePlayersAtEnd->setChecked(settings.Knockout.RevivePlayersAtEnd);
	m_LiveSort->setChecked(settings.Knockout.LiveSort);
	m_HideOverlayOnBreaks->setChecked(settings.Knockout.HideOverlayOnBreaks);
	m_MinCursorSize->setValue(settings.Knockout.MinCursorSize);
	m_MaxCursorSize->setValue(settings.Knockout.MaxCursorSize);
	m_AddDanser->setChecked(settings.Knockout.AddDanser);
	m_DanserName->setText(settings.Knockout.DanserName);
}

void KnockoutSettingsWidget::WriteSettings(Settings& settings) const
{
	settings.Knockout.Mode = m_Mode->currentIndex();
	settings.Knockout.SortBy = m_SortBy->currentText();
	settings.Knockout.GraceEndTime = m_GraceEndTime->value();
	settings.Knockout.BubbleMinimumCombo = m_BubbleMinimumCombo->value();
	settings.Knockout.MaxPlayers = m_MaxPlayers->value();
	settings.Knockout.RevivePlayersAtEnd = m_RevivePlayersAtEnd->isChecked();
	settings.Knockout.LiveSort = m_LiveSort->isChecked();
	settings.Knockout.HideOverlayOnBreaks = m_HideOverlayOnBreaks->isChecked();
	settings.Knockout.MinCursorSize = m_MinCursorSize->value();
	settings.Knockout.MaxCursorSize = m_MaxCursorSize->value();
	settings.Knockout.AddDanser = m_AddDanser->isChecked();
	settings.Knockout.DanserName = m_DanserName->text();
}
