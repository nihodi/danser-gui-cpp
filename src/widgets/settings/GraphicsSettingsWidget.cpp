#include "GraphicsSettingsWidget.h"

#include <iostream>

GraphicsSettingsWidget::GraphicsSettingsWidget(QWidget* parent)
	: QWidget(parent)
{

	m_GroupLayout = new QGridLayout;
	m_Layout = new QGridLayout;

	m_FullscreenWidthInput = new QSpinBox;
	m_FullscreenHeightInput = new QSpinBox;
	m_WindowedWidthInput = new QSpinBox;
	m_WindowedHeightInput = new QSpinBox;
	m_FPSCapInput = new QSpinBox;

	m_FullscreenWidthInput->setRange(1, 100000);
	m_FullscreenHeightInput->setRange(1, 100000);
	m_WindowedWidthInput->setRange(1, 100000);
	m_WindowedHeightInput->setRange(1, 100000);
	m_FPSCapInput->setRange(0, 100000);

	m_FullscreenWidthInput->setToolTip("Width of the window in fullscreen mode");
	m_FullscreenHeightInput->setToolTip("Height of the window in fullscreen mode");
	m_WindowedWidthInput->setToolTip("Width of the window in windowed mode");
	m_WindowedHeightInput->setToolTip("Height of the window in windowed mode");
	m_FPSCapInput->setToolTip("FPS Cap. 0 means unlimited.");

	m_FullscreenInput = new QCheckBox;
	m_VSyncInput = new QCheckBox;
	m_ShowFPS = new QCheckBox;

	m_FullscreenInput->setToolTip("Enable/disable fullscreen mode. Double-check window dimensions!");
	m_VSyncInput->setToolTip("Enable/disable VSync. Overrides FPS Cap setting.");
	m_ShowFPS->setToolTip("Enable/disable frame statistics in the lower-right corner.");

	m_GroupLayout->addWidget(m_FullscreenWidthInput, 0, 1);
	m_GroupLayout->addWidget(m_FullscreenHeightInput, 1, 1);
	m_GroupLayout->addWidget(m_WindowedWidthInput, 2, 1);
	m_GroupLayout->addWidget(m_WindowedHeightInput, 3, 1);

	m_GroupLayout->addWidget(m_FullscreenInput, 4, 1);
	m_GroupLayout->addWidget(m_VSyncInput, 5, 1);

	m_GroupLayout->addWidget(m_FPSCapInput, 6, 1);
	m_GroupLayout->addWidget(m_ShowFPS, 7, 1);

	auto fWidth = new QLabel("Fullscreen Width");
	auto fHeight = new QLabel("Fullscreen Height");
	auto wWidth = new QLabel("Windowed Width");
	auto wHeight = new QLabel("Windowed Height");
	auto fullscreen = new QLabel("Fullscreen");
	auto vsync = new QLabel("VSync");
	auto fpsCap = new QLabel("FPS Cap");
	auto showFPS = new QLabel("Show FPS");

	fWidth->setToolTip(m_FullscreenWidthInput->toolTip());
	fHeight->setToolTip(m_FullscreenHeightInput->toolTip());
	wWidth->setToolTip(m_WindowedWidthInput->toolTip());
	wHeight->setToolTip(m_WindowedHeightInput->toolTip());
	fullscreen->setToolTip(m_FullscreenInput->toolTip());
	vsync->setToolTip(m_VSyncInput->toolTip());
	fpsCap->setToolTip(m_FPSCapInput->toolTip());
	showFPS->setToolTip(m_ShowFPS->toolTip());

	m_GroupLayout->addWidget(fWidth, 0, 0);
	m_GroupLayout->addWidget(fHeight, 1, 0);
	m_GroupLayout->addWidget(wWidth, 2, 0);
	m_GroupLayout->addWidget(wHeight, 3, 0);
	m_GroupLayout->addWidget(fullscreen, 4, 0);
	m_GroupLayout->addWidget(vsync, 5, 0);
	m_GroupLayout->addWidget(fpsCap, 6, 0);
	m_GroupLayout->addWidget(showFPS, 7, 0);

	m_GroupBox = new QGroupBox;
	m_GroupBox->setLayout(m_GroupLayout);

	m_Layout->addWidget(m_GroupBox);
	setLayout(m_Layout);
	setMaximumHeight(500);
}

void GraphicsSettingsWidget::ReadSettings(const Settings& settings)
{
	m_FullscreenWidthInput->setValue(settings.Graphics.Width);
	m_FullscreenHeightInput->setValue(settings.Graphics.Height);
	m_WindowedWidthInput->setValue(settings.Graphics.WindowWidth);
	m_WindowedHeightInput->setValue(settings.Graphics.WindowHeight);

	m_FullscreenInput->setChecked(settings.Graphics.Fullscreen);
	m_VSyncInput->setChecked(settings.Graphics.VSync);
	m_ShowFPS->setChecked(settings.Graphics.ShowFPS);

	m_FPSCapInput->setValue(settings.Graphics.FPSCap);
}

void GraphicsSettingsWidget::WriteSettings(Settings& settings) const
{
	settings.Graphics.Width = m_FullscreenWidthInput->text().toInt();
	settings.Graphics.Height = m_FullscreenHeightInput->text().toInt();
	settings.Graphics.WindowWidth = m_WindowedWidthInput->text().toInt();
	settings.Graphics.WindowHeight = m_WindowedHeightInput->text().toInt();

	settings.Graphics.Fullscreen = m_FullscreenInput->isChecked();
	settings.Graphics.VSync = m_VSyncInput->isChecked();
	settings.Graphics.ShowFPS = m_ShowFPS->isChecked();

	settings.Graphics.FPSCap = m_FPSCapInput->text().toInt();

}
