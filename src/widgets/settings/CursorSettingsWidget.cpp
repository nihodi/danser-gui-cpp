#include "CursorSettingsWidget.h"

CursorSettingsWidget::CursorSettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout;

	CreateTrailGroupBox();
	CreateCursorGroup();
	m_Layout->addWidget(m_CursorGroupBox, 0, 0);
	m_Layout->addWidget(m_TrailGroupBox, 1, 0);

	setLayout(m_Layout);
	setMaximumHeight(2500);
}

void CursorSettingsWidget::CreateTrailGroupBox()
{
	m_TrailGroupBox = new QGroupBox("Cursor Trail");
	m_TrailGroupBoxLayout = new QGridLayout;

	m_GlowGroupBox = new QGroupBox("Trail Glow");
	auto glowLayout = new QGridLayout;
	m_GlowGroupBox->setCheckable(true);
	m_GlowGroupBox->setLayout(glowLayout);

	// trail style
	m_TrailStyle = new QComboBox;
	m_TrailStyle->addItem("Unified color");
	m_TrailStyle->addItem("Distance-based rainbow");
	m_TrailStyle->addItem("Time-based rainbow");
	m_TrailStyle->addItem("Constant Gradient");
	m_TrailStyle->setToolTip("Unified color: A single color for the entire trail. Changes over time.\n"
							 "Distance-based rainbow: The trail changes colors in a rainbow fashion. The colors change based on how far the cursor has moved.\n"
							 "Time-based rainbow: Same as above, but the colors change at a fixed rate.\n"
							 "Constant Gradient: The colors are decided by a constant gradient.");

	// trail style style (lol)
	m_Style23Speed = new QDoubleSpinBox;
	m_Style23Speed->setDecimals(2);
	m_Style23Speed->setRange(-100000, 100000);
	m_Style23Speed->setSingleStep(0.25);
	m_Style23Speed->setToolTip("ONLY APPLIES TO Distance-based rainbow AND Time-based rainbow.\n"
							   "The speed of colors changing.");

	m_Style4Shift = new QDoubleSpinBox;
	m_Style4Shift->setDecimals(2);
	m_Style4Shift->setRange(-100000, 100000);
	m_Style4Shift->setSingleStep(0.1);
	m_Style4Shift->setToolTip("Determines the shift for style Constant Gradient.\n"
							  "Value of 1 means that hue at the end of the trail will be shifted by 360°");

	// trail glow
	m_EnableCustomTrailGlowOffset = new QCheckBox;
	m_EnableCustomTrailGlowOffset->setToolTip("Enable/Disable custom trail glow offset.");

	m_CustomTrailGlowOffset = new QSpinBox;
	m_CustomTrailGlowOffset->setRange(-100000, 100000);
	m_CustomTrailGlowOffset->setSingleStep(1);
	m_CustomTrailGlowOffset->setToolTip("Determines the custom trail glow offset (° of H)");

	// trail scale and glow
	m_TrailScale = new QDoubleSpinBox;
	m_TrailEndScale = new QDoubleSpinBox;
	m_TrailDensity = new QDoubleSpinBox;
	m_TrailMaxLength = new QSpinBox;
	m_TrailRemoveSpeed = new QDoubleSpinBox;
	m_GlowEndScale = new QDoubleSpinBox;
	m_InnerLengthMult = new QDoubleSpinBox;
	m_AdditiveBlending = new QCheckBox;

	m_TrailScale->setToolTip("Determines the scale multiplier for the cursor trail.");
	m_TrailEndScale->setToolTip("Determines the scale multiplier for the cursor trail end.");
	m_TrailDensity->setToolTip("Determines the density for the cursor trail.\nHigher value → smoother trail.");
	m_TrailMaxLength->setToolTip("Determines the maximum length for the cursor trail.");
	m_TrailRemoveSpeed->setToolTip("Determines the removal speed for the cursor trail.");
	m_GlowEndScale->setToolTip("Determines the scale for the cursor trail glow end."
							   "\nHigher value → Bigger glow.");
	m_InnerLengthMult->setToolTip("Multiplier for the cursor trail inner length.");
	m_AdditiveBlending->setToolTip("Enable/Disable additive blending for the cursor trail."
								   "\nMost noticable in knockouts.");

	m_TrailScale->setSingleStep(0.05);
	m_TrailEndScale->setSingleStep(0.05);
	m_TrailDensity->setSingleStep(0.05);
	m_TrailRemoveSpeed->setSingleStep(0.05);
	m_GlowEndScale->setSingleStep(0.05);
	m_InnerLengthMult->setSingleStep(0.05);

	m_TrailMaxLength->setRange(0.0, 100000000.0);

	auto gOffset = new QLabel("Custom trail glow offset");
	gOffset->setToolTip(m_CustomTrailGlowOffset->toolTip());

	glowLayout->addWidget(gOffset, 0, 0);

	glowLayout->addWidget(m_EnableCustomTrailGlowOffset, 0, 1, Qt::AlignHCenter);
	glowLayout->addWidget(m_CustomTrailGlowOffset, 0, 2, 1, 2);

	auto tStyle = new QLabel("Trail style");
	auto style23 = new QLabel("Distance-based rainbow and Time-based rainbow speed");
	auto style4 = new QLabel("Constant Gradient shift amount");
	tStyle->setToolTip(m_TrailStyle->toolTip());
	style23->setToolTip(m_Style23Speed->toolTip());
	style4->setToolTip(m_Style4Shift->toolTip());

	m_TrailGroupBoxLayout->addWidget(tStyle, 0, 0);
	m_TrailGroupBoxLayout->addWidget(style23, 1, 0);
	m_TrailGroupBoxLayout->addWidget(style4, 2, 0);

	m_TrailGroupBoxLayout->addWidget(m_GlowGroupBox, 3, 0, 1, 2);

	auto tScale = new QLabel("Trail scale");
	auto tEndScale = new QLabel("Trail end scale");
	auto tDensity = new QLabel("Trail density");
	auto tMaxLen = new QLabel("Max trail length");
	auto tRemSpeed = new QLabel("Disappearance speed");
	auto tTrailGlowEndScale = new QLabel("Cursor trail glow end scale");
	auto tInnerLength = new QLabel("Trail inner length multiplier");
	auto tBlendTrail = new QLabel("Blend cursor trail colors");

	tScale->setToolTip(m_TrailScale->toolTip());
	tEndScale->setToolTip(m_TrailEndScale->toolTip());
	tDensity->setToolTip(m_TrailDensity->toolTip());
	tMaxLen->setToolTip(m_TrailMaxLength->toolTip());
	tRemSpeed->setToolTip(m_TrailRemoveSpeed->toolTip());
	tTrailGlowEndScale->setToolTip(m_GlowEndScale->toolTip());
	tInnerLength->setToolTip(m_InnerLengthMult->toolTip());
	tBlendTrail->setToolTip(m_AdditiveBlending->toolTip());

	m_TrailGroupBoxLayout->addWidget(tScale, 6, 0);
	m_TrailGroupBoxLayout->addWidget(tEndScale, 7, 0);
	m_TrailGroupBoxLayout->addWidget(tDensity, 8, 0);
	m_TrailGroupBoxLayout->addWidget(tMaxLen, 9, 0);
	m_TrailGroupBoxLayout->addWidget(tRemSpeed, 10, 0);
	m_TrailGroupBoxLayout->addWidget(tTrailGlowEndScale, 11, 0);
	m_TrailGroupBoxLayout->addWidget(tInnerLength, 12, 0);
	m_TrailGroupBoxLayout->addWidget(tBlendTrail, 13, 0);

	m_TrailGroupBoxLayout->addWidget(m_TrailStyle, 0, 1);
	m_TrailGroupBoxLayout->addWidget(m_Style23Speed, 1, 1);
	m_TrailGroupBoxLayout->addWidget(m_Style4Shift, 2, 1);

	m_TrailGroupBoxLayout->addWidget(m_TrailScale, 6, 1);
	m_TrailGroupBoxLayout->addWidget(m_TrailEndScale, 7, 1);
	m_TrailGroupBoxLayout->addWidget(m_TrailDensity, 8, 1);
	m_TrailGroupBoxLayout->addWidget(m_TrailMaxLength, 9, 1);
	m_TrailGroupBoxLayout->addWidget(m_TrailRemoveSpeed, 10, 1);
	m_TrailGroupBoxLayout->addWidget(m_GlowEndScale, 11, 1);
	m_TrailGroupBoxLayout->addWidget(m_InnerLengthMult, 12, 1);
	m_TrailGroupBoxLayout->addWidget(m_AdditiveBlending, 13, 1);

	m_TrailGroupBox->setLayout(m_TrailGroupBoxLayout);

	connect(m_EnableCustomTrailGlowOffset, SIGNAL(clicked(bool)), m_CustomTrailGlowOffset, SLOT(setEnabled(bool)));
}

void CursorSettingsWidget::CreateCursorGroup()
{
	m_CursorGroupBox = new QGroupBox("Cursor");
	m_CursorGroupBoxLayout = new QGridLayout;
	m_CursorGroupBox->setLayout(m_CursorGroupBoxLayout);

	m_CursorSize = new QSpinBox;
	m_CursorSize->setRange(0, 1000);
	m_CursorSize->setToolTip("Determines the size of the cursor");

	m_CursorExpand = new QCheckBox;
	m_CursorRipples = new QCheckBox;
	m_ScaleToBeat = new QCheckBox;
	m_ShowCursorOnBreaks = new QCheckBox;
	m_BounceOnEdges = new QCheckBox;
	m_SmokeEnabled = new QCheckBox;

	m_CursorExpand->setToolTip("Enable/Disable cursor expanding on click.");
	m_CursorRipples->setToolTip("Enable/disable cursor ripples."
								"\nNote: Ripples are only visible in the background.");
	m_ScaleToBeat->setToolTip("Enable/Disable scaling the cursor to the beat.");
	m_ShowCursorOnBreaks->setToolTip("Shows/Hides the cursor on breaks."
									 "\nIf bouncing on edges in enabled, the cursor will bounce around on the screen.");
	m_BounceOnEdges->setToolTip("Enables/Disables cursor bouncing on edges.");
	m_SmokeEnabled->setToolTip("Enable seeing smoke when viewing replays.");

	// cursor colors things
	m_CursorColorsGroupBox = new QGroupBox("Cursor Style");
	m_CursorColorsLayout = new QGridLayout;
	m_CursorColorsGroupBox->setLayout(m_CursorColorsLayout);

	m_EnableCursorRainbow = new QCheckBox;
	m_CursorFlashToTheBeat = new QCheckBox;
	m_EnableCursorRainbow->setToolTip("Enable/Disable rainbow for the cursor.");
	m_CursorFlashToTheBeat->setToolTip("Enable/Disable flashing to the beat.");

	m_CursorRainbowSpeed = new QDoubleSpinBox;
	m_CursorRainbowSpeed->setRange(-1000, 1000);
	m_CursorRainbowSpeed->setSingleStep(0.25);
	m_CursorFlashAmplitude = new QDoubleSpinBox;
	m_CursorFlashAmplitude->setRange(0, 360);
	m_CursorFlashAmplitude->setSingleStep(0.25);

	m_CursorRainbowSpeed->setToolTip("Determines the speed for the rainbow (°/s of H).");
	m_CursorFlashAmplitude->setToolTip("Determines the amplitude for the flashing to the beat (° of H).");

	auto cRainbow = new QLabel("Enable cursor rainbow");
	auto cRSpeed = new QLabel("Cursor rainbow speed");
	auto cFlash = new QLabel("Cursor flash to the beat");
	auto cFAmplitude = new QLabel("Cursor flash to amplitude");
	cRainbow->setToolTip(m_EnableCursorRainbow->toolTip());
	cRSpeed->setToolTip(m_CursorRainbowSpeed->toolTip());
	cFlash->setToolTip(m_CursorFlashToTheBeat->toolTip());
	cFAmplitude->setToolTip(m_CursorFlashAmplitude->toolTip());

	m_CursorColorsLayout->addWidget(cRainbow, 0, 0);
	m_CursorColorsLayout->addWidget(cRSpeed, 1, 0);
	m_CursorColorsLayout->addWidget(cFlash, 2, 0);
	m_CursorColorsLayout->addWidget(cFAmplitude, 3, 0);
	m_CursorColorsLayout->addWidget(m_EnableCursorRainbow, 0, 1);
	m_CursorColorsLayout->addWidget(m_CursorRainbowSpeed, 1, 1);
	m_CursorColorsLayout->addWidget(m_CursorFlashToTheBeat, 2, 1);
	m_CursorColorsLayout->addWidget(m_CursorFlashAmplitude, 3, 1);

	auto cSize = new QLabel("Cursor size");
	auto cExpand = new QLabel("Expand cursor on click");
	auto cRipples = new QLabel("Cursor ripples on click");
	auto cScaleBeat = new QLabel("Scale cursor to the beat");
	auto cBreaks = new QLabel("Show cursor on breaks");
	auto cBounce = new QLabel("Bounce on screen edges");
	auto cSmoke = new QLabel("Enable smoke");
	cSize->setToolTip(m_CursorSize->toolTip());
	cExpand->setToolTip(m_CursorExpand->toolTip());
	cRipples->setToolTip(m_CursorRipples->toolTip());
	cScaleBeat->setToolTip(m_ScaleToBeat->toolTip());
	cBreaks->setToolTip(m_ShowCursorOnBreaks->toolTip());
	cBounce->setToolTip(m_BounceOnEdges->toolTip());
	cSmoke->setToolTip(m_SmokeEnabled->toolTip());

	m_CursorGroupBoxLayout->addWidget(cSize, 0, 0);
	m_CursorGroupBoxLayout->addWidget(cExpand, 1, 0);
	m_CursorGroupBoxLayout->addWidget(cRipples, 2, 0);
	m_CursorGroupBoxLayout->addWidget(cScaleBeat, 3, 0);
	m_CursorGroupBoxLayout->addWidget(cBreaks, 4, 0);
	m_CursorGroupBoxLayout->addWidget(cBounce, 5, 0);
	m_CursorGroupBoxLayout->addWidget(cSmoke, 6, 0);

	m_CursorGroupBoxLayout->addWidget(m_CursorSize, 0, 1);

	m_CursorGroupBoxLayout->addWidget(m_CursorExpand, 1, 1);
	m_CursorGroupBoxLayout->addWidget(m_CursorRipples, 2, 1);
	m_CursorGroupBoxLayout->addWidget(m_ScaleToBeat, 3, 1);
	m_CursorGroupBoxLayout->addWidget(m_ShowCursorOnBreaks, 4, 1);
	m_CursorGroupBoxLayout->addWidget(m_BounceOnEdges, 5, 1);
	m_CursorGroupBoxLayout->addWidget(m_SmokeEnabled, 6, 1);

	m_CursorGroupBoxLayout->addWidget(m_CursorColorsGroupBox, 6, 0, 1, 2);

	connect(m_EnableCursorRainbow, SIGNAL(clicked(bool)), m_CursorRainbowSpeed, SLOT(setEnabled(bool)));
	connect(m_CursorFlashToTheBeat, SIGNAL(clicked(bool)), m_CursorFlashAmplitude, SLOT(setEnabled(bool)));
}

void CursorSettingsWidget::ReadSettings(const Settings& settings)
{
	// cursor settings
	m_CursorSize->setValue(settings.Cursor.CursorSize);
	m_CursorExpand->setChecked(settings.Cursor.CursorExpand);
	//m_CursorRipples->setChecked(settings.Cursor.CursorRipples);
	m_ScaleToBeat->setChecked(settings.Cursor.ScaleToTheBeat);
	m_ShowCursorOnBreaks->setChecked(settings.Cursor.ShowCursorsOnBreaks);
	m_BounceOnEdges->setChecked(settings.Cursor.BounceOnEdges);
	m_SmokeEnabled->setChecked(settings.Cursor.SmokeEnabled);

	m_EnableCursorRainbow->setChecked(settings.Cursor.Colors.EnableRainbow);
	m_CursorFlashToTheBeat->setChecked(settings.Cursor.Colors.FlashToTheBeat);

	m_CursorRainbowSpeed->setValue(settings.Cursor.Colors.RainbowSpeed);
	m_CursorRainbowSpeed->setEnabled(settings.Cursor.Colors.EnableRainbow);
	m_CursorFlashAmplitude->setValue(settings.Cursor.Colors.FlashAmplitude);
	m_CursorFlashAmplitude->setEnabled(settings.Cursor.Colors.FlashToTheBeat);


	// cursor style

	// trail settings
	m_TrailStyle->setCurrentIndex(settings.Cursor.TrailStyle - 1);

	m_Style23Speed->setValue(settings.Cursor.Style23Speed);
	m_Style4Shift->setValue(settings.Cursor.Style4Shift);

	// trail glow
	m_GlowGroupBox->setChecked(settings.Cursor.EnableTrailGlow);
	m_EnableCustomTrailGlowOffset->setChecked(settings.Cursor.EnableCustomTrailGlowOffset);

	m_CustomTrailGlowOffset->setValue(settings.Cursor.TrailGlowOffset);
	m_CustomTrailGlowOffset->setEnabled(settings.Cursor.EnableCustomTrailGlowOffset);

	// trail scale, length and glow
	m_TrailScale->setValue(settings.Cursor.TrailScale);
	m_TrailEndScale->setValue(settings.Cursor.TrailEndScale);
	m_TrailDensity->setValue(settings.Cursor.TrailDensity);
	m_TrailMaxLength->setValue(settings.Cursor.TrailMaxLength);
	m_TrailRemoveSpeed->setValue(settings.Cursor.TrailRemoveSpeed);
	m_GlowEndScale->setValue(settings.Cursor.GlowEndScale);
	m_InnerLengthMult->setValue(settings.Cursor.InnerLengthMult);

	m_AdditiveBlending->setChecked(settings.Cursor.AdditiveBlending);
}

void CursorSettingsWidget::WriteSettings(Settings& settings)
{
	// cursor settings
	settings.Cursor.CursorSize = m_CursorSize->value();

	settings.Cursor.CursorExpand = m_CursorExpand->isChecked();
	//settings.Cursor.CursorRipples = m_CursorRipples->isChecked();
	settings.Cursor.ScaleToTheBeat = m_ScaleToBeat->isChecked();
	settings.Cursor.ShowCursorsOnBreaks = m_ShowCursorOnBreaks->isChecked();
	settings.Cursor.BounceOnEdges = m_BounceOnEdges->isChecked();
	settings.Cursor.SmokeEnabled = m_SmokeEnabled->isChecked();

	// trail settings
	settings.Cursor.TrailStyle = m_TrailStyle->currentIndex() + 1;

	settings.Cursor.Style23Speed = m_Style23Speed->value();
	settings.Cursor.Style4Shift = m_Style4Shift->value();

	// trail glow
	settings.Cursor.EnableTrailGlow = m_GlowGroupBox->isChecked();
	settings.Cursor.EnableCustomTrailGlowOffset = m_EnableCustomTrailGlowOffset->isChecked();
	settings.Cursor.TrailGlowOffset = m_CustomTrailGlowOffset->value();

	// trail scale, length and glow
	settings.Cursor.TrailScale = m_TrailScale->value();
	settings.Cursor.TrailEndScale = m_TrailEndScale->value();
	settings.Cursor.TrailDensity = m_TrailDensity->value();
	settings.Cursor.TrailMaxLength = m_TrailMaxLength->value();
	settings.Cursor.TrailRemoveSpeed = m_TrailRemoveSpeed->value();
	settings.Cursor.GlowEndScale = m_GlowEndScale->value();
	settings.Cursor.InnerLengthMult = m_InnerLengthMult->value();

	settings.Cursor.AdditiveBlending = m_AdditiveBlending->isChecked();
}
