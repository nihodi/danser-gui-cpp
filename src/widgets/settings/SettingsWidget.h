#pragma once

#include <QWidget>
#include <QTabWidget>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include <QGridLayout>
#include <QPushButton>

#include "GraphicsSettingsWidget.h"
#include "AudioSettingsWidget.h"
#include "BackgroundSettingsWidget.h"
#include "GeneralSettingsWidget.h"
#include "CursorSettingsWidget.h"
#include "SkinSettingsWidget.h"
#include "KnockoutSettingsWidget.h"

#include "io/Settings.h"

class SettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit SettingsWidget(QWidget* parent = nullptr);

private slots:
	void SaveSettingsButtonPressed();
	void TabChanged(int index);

private:
	void readSettingsfile(const std::string& filepath);
	void writeSettingsFile(const std::string& filepath);

	QTabWidget* m_TabWidget;

	GraphicsSettingsWidget* m_GraphicsSettingsWidget;
	AudioSettingsWidget* m_AudioSettingsWidget;
	BackgroundSettingsWidget* m_BackgroundSettingsWidget;
	GeneralSettingsWidget* m_GeneralSettingsWidget;
	CursorSettingsWidget* m_CursorSettingsWidget;
	SkinSettingsWidget* m_SkinSettingsWidget;
	KnockoutSettingsWidget* m_KnockoutSettingsWidget;

	QGridLayout* m_Layout;
	QPushButton* m_SaveSettingsButton;

	std::string m_SettingsFilePath;

};
