#pragma once

#include <QWidget>
#include <QGroupBox>
#include <QGridLayout>

#include <QLabel>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QCheckBox>

#include "io/Settings.h"

class CursorSettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit CursorSettingsWidget(QWidget* parent = nullptr);

	void ReadSettings(const Settings& settings);
	void WriteSettings(Settings& settings);

private:
	void CreateTrailGroupBox();
	void CreateCursorGroup();

	QGridLayout* m_Layout;

	// trail
	QGroupBox* m_TrailGroupBox;
	QGridLayout* m_TrailGroupBoxLayout;

	QGroupBox* m_GlowGroupBox;

	QComboBox* m_TrailStyle;
	QDoubleSpinBox* m_Style23Speed;
	QDoubleSpinBox* m_Style4Shift;

	QCheckBox* m_EnableCustomTrailGlowOffset;
	QSpinBox* m_CustomTrailGlowOffset;

	QDoubleSpinBox* m_TrailScale;
	QDoubleSpinBox* m_TrailEndScale;
	QDoubleSpinBox* m_TrailDensity;
	QSpinBox* m_TrailMaxLength;
	QDoubleSpinBox* m_TrailRemoveSpeed;
	QDoubleSpinBox* m_GlowEndScale;
	QDoubleSpinBox* m_InnerLengthMult;

	QCheckBox* m_AdditiveBlending;

	// cursor
	QGroupBox* m_CursorGroupBox;
	QGridLayout* m_CursorGroupBoxLayout;


	QSpinBox* m_CursorSize;
	QCheckBox* m_CursorExpand;
	QCheckBox* m_ScaleToBeat;
	QCheckBox* m_ShowCursorOnBreaks;
	QCheckBox* m_BounceOnEdges;
	QCheckBox* m_CursorRipples;
	QCheckBox* m_SmokeEnabled;

	// CURSOR (not trail) colors
	QGroupBox* m_CursorColorsGroupBox;
	QGridLayout* m_CursorColorsLayout;

	QCheckBox* m_EnableCursorRainbow;
	QDoubleSpinBox* m_CursorRainbowSpeed;

	// base color
	// TODO: this maybe some day?
	QGroupBox* m_CursorBaseColorGroupBox;
	QGridLayout* m_CursorBaseColorLayout;

	QSpinBox* m_CursorBaseColorH;
	QSpinBox* m_CursorBaseColorS;
	QSpinBox* m_CursorBaseColorV;

	QCheckBox* m_CursorFlashToTheBeat;
	QDoubleSpinBox* m_CursorFlashAmplitude;
};
