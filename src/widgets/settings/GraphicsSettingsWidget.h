#pragma once

#include <QWidget>
#include <QGroupBox>
#include <QGridLayout>

#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QSpinBox>

#include <QJsonDocument>
#include <QJsonObject>

#include "io/Settings.h"

class GraphicsSettingsWidget : public QWidget
{

	Q_OBJECT

public:
	explicit GraphicsSettingsWidget(QWidget* parent = nullptr);

	void ReadSettings(const Settings& settings);
	void WriteSettings(Settings& settings) const;

private:
	QGridLayout* m_Layout;

	QGroupBox* m_GroupBox;
	QGridLayout* m_GroupLayout;

	QSpinBox* m_FullscreenWidthInput;
	QSpinBox* m_FullscreenHeightInput;
	QSpinBox* m_WindowedWidthInput;
	QSpinBox* m_WindowedHeightInput;

	QCheckBox* m_FullscreenInput;
	QCheckBox* m_VSyncInput;

	QSpinBox* m_FPSCapInput;
	QCheckBox* m_ShowFPS;



};
