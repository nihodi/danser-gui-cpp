#include "BackgroundSettingsWidget.h"

BackgroundSettingsWidget::BackgroundSettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout;

	// General
	QGroupBox* videoGroupBox = new QGroupBox("General");
	QGridLayout* videoLayout = new QGridLayout;

	m_LoadStoryboards = new QCheckBox;
	m_LoadVideos = new QCheckBox;
	m_FlashToTheBeat = new QCheckBox;

	m_LoadStoryboards->setToolTip("Enable/disable loading storyboards.");
	m_LoadVideos->setToolTip("Enable/disable loading videos.");
	m_FlashToTheBeat->setToolTip("Enable/disable background slightly flash to the beat.");

	auto storyboard = new QLabel("Load Storyboards");
	auto video = new QLabel("Load Videos");
	auto flashBG =new QLabel("Flash to the beat");

	storyboard->setToolTip(m_LoadStoryboards->toolTip());
	video->setToolTip(m_LoadVideos->toolTip());
	flashBG->setToolTip(m_FlashToTheBeat->toolTip());

	videoLayout->addWidget(storyboard, 0, 0);
	videoLayout->addWidget(video, 1, 0);
	videoLayout->addWidget(flashBG, 2, 0);
	videoLayout->addWidget(m_LoadStoryboards, 0, 1);
	videoLayout->addWidget(m_LoadVideos, 1, 1);
	videoLayout->addWidget(m_FlashToTheBeat, 2, 1);
	videoGroupBox->setLayout(videoLayout);

	CreateDimGroupBox();
	CreateParallaxGroupBox();
	CreateBlurGroupBox();
	CreateTrianglesGroupBox();
	m_Layout->addWidget(videoGroupBox, 0, 0);
	m_Layout->addWidget(m_DimGroupBox, 1, 0);
	m_Layout->addWidget(m_ParallaxGroupBox, 2, 0);
	m_Layout->addWidget(m_BlurGroupBox, 3, 0);
	m_Layout->addWidget(m_TrianglesGroupBox, 4, 0);

	setLayout(m_Layout);

	setMaximumHeight(1000);
}

void BackgroundSettingsWidget::CreateDimGroupBox()
{
	m_DimGroupBox = new QGroupBox("Dim");
	m_DimLayout = new QGridLayout;

	// create and add stuff to m_DimLayout

	// dim sliders
	m_IntroDimSlider = new QSlider(Qt::Horizontal);
	m_IntroDimSlider->setRange(0, 100);
	m_IntroDimSlider->setSingleStep(1);
	m_IntroDimSlider->setToolTip("Dim before objects start appearing.");

	m_GameplayDimSlider = new QSlider(Qt::Horizontal);
	m_GameplayDimSlider->setRange(0, 100);
	m_GameplayDimSlider->setSingleStep(1);
	m_GameplayDimSlider->setToolTip("Dim for when objects are visible (drain time).");

	m_BreakDimSlider = new QSlider(Qt::Horizontal);
	m_BreakDimSlider->setRange(0, 100);
	m_BreakDimSlider->setSingleStep(1);
	m_BreakDimSlider->setToolTip("Dim during breaks");

	// dim spin boxes
	m_IntroDimSpinBox = new QDoubleSpinBox;
	m_IntroDimSpinBox->setRange(0.0, 1.0);
	m_IntroDimSpinBox->setSingleStep(0.05);
	m_IntroDimSpinBox->setDecimals(2);
	m_IntroDimSpinBox->setToolTip(m_IntroDimSlider->toolTip());

	m_GameplayDimSpinBox = new QDoubleSpinBox;
	m_GameplayDimSpinBox->setRange(0.0, 1.0);
	m_GameplayDimSpinBox->setSingleStep(0.05);
	m_GameplayDimSpinBox->setDecimals(2);
	m_GameplayDimSpinBox->setToolTip(m_GameplayDimSlider->toolTip());

	m_BreakDimSpinBox = new QDoubleSpinBox;
	m_BreakDimSpinBox->setRange(0.0, 1.0);
	m_BreakDimSpinBox->setSingleStep(0.05);
	m_BreakDimSpinBox->setDecimals(2);
	m_BreakDimSpinBox->setToolTip(m_BreakDimSlider->toolTip());

	// add dim to layout
	auto iDim = new QLabel("Background dim during intro");
	auto gDim = new QLabel("Background dim during gameplay");
	auto bDim = new QLabel("Background dim during breaks");
	iDim->setToolTip(m_IntroDimSlider->toolTip());
	gDim->setToolTip(m_GameplayDimSlider->toolTip());
	bDim->setToolTip(m_BreakDimSlider->toolTip());

	m_DimLayout->addWidget(iDim, 0, 0);
	m_DimLayout->addWidget(gDim, 1, 0);
	m_DimLayout->addWidget(bDim, 2, 0);

	m_DimLayout->addWidget(m_IntroDimSlider, 0, 1);
	m_DimLayout->addWidget(m_GameplayDimSlider, 1, 1);
	m_DimLayout->addWidget(m_BreakDimSlider, 2, 1);

	m_DimLayout->addWidget(m_IntroDimSpinBox, 0, 2);
	m_DimLayout->addWidget(m_GameplayDimSpinBox, 1, 2);
	m_DimLayout->addWidget(m_BreakDimSpinBox, 2, 2);

	// connect things

	connect(m_IntroDimSlider, SIGNAL(valueChanged(int)), this, SLOT(IntroDimSliderChanged(int)));
	connect(m_GameplayDimSlider, SIGNAL(valueChanged(int)), this, SLOT(GameplayDimSliderChanged(int)));
	connect(m_BreakDimSlider, SIGNAL(valueChanged(int)), this, SLOT(BreakDimSliderChanged(int)));

	connect(m_IntroDimSpinBox, SIGNAL(valueChanged(double)), this, SLOT(IntroDimSpinBoxChanged(double)));
	connect(m_GameplayDimSpinBox, SIGNAL(valueChanged(double)), this, SLOT(GameplayDimSpinBoxChanged(double)));
	connect(m_BreakDimSpinBox, SIGNAL(valueChanged(double)), this, SLOT(BreakDimSpinBoxChanged(double)));

	m_DimGroupBox->setLayout(m_DimLayout);
}

void BackgroundSettingsWidget::CreateParallaxGroupBox()
{
	m_ParallaxGroupBox = new QGroupBox("Parallax");

	m_ParallaxLayout = new QGridLayout;

	m_ParallaxAmount = new QDoubleSpinBox;
	m_ParallaxAmount->setRange(0.0, 1.0);
	m_ParallaxAmount->setSingleStep(0.05);
	m_ParallaxAmount->setDecimals(2);
	m_ParallaxAmount->setToolTip("Multiplier of cursors distance to screen center. More → background following cursor at a greater scale.");

	m_ParallaxSpeed = new QDoubleSpinBox;
	m_ParallaxSpeed->setRange(0.0, 1.0);
	m_ParallaxSpeed->setSingleStep(0.05);
	m_ParallaxSpeed->setDecimals(2);
	m_ParallaxSpeed->setToolTip("Speed of the parallax movement.1 means it's the same as cursor.");

	auto pAmount = new QLabel("Parallax amount");
	auto pSpeed = new QLabel("Parallax speed");
	pAmount->setToolTip(m_ParallaxAmount->toolTip());
	pSpeed->setToolTip(m_ParallaxSpeed->toolTip());

	m_ParallaxLayout->addWidget(pAmount, 0, 0);
	m_ParallaxLayout->addWidget(pSpeed, 1, 0);
	m_ParallaxLayout->addWidget(m_ParallaxAmount, 0, 1);
	m_ParallaxLayout->addWidget(m_ParallaxSpeed, 1, 1);

	m_ParallaxGroupBox->setLayout(m_ParallaxLayout);
}

void BackgroundSettingsWidget::CreateBlurGroupBox()
{
	m_BlurGroupBox = new QGroupBox("Blur");
	m_BlurGroupBox->setCheckable(true);
	m_BlurGroupBox->setChecked(false);

	m_BlurLayout = new QGridLayout;

	m_IntroBlurSpinBox = new QDoubleSpinBox;
	m_IntroBlurSpinBox->setRange(0.0, 1.0);
	m_IntroBlurSpinBox->setSingleStep(0.05);
	m_IntroBlurSpinBox->setDecimals(2);
	m_IntroBlurSpinBox->setToolTip("Blur before objects start appearing.");

	m_GameplayBlurSpinBox = new QDoubleSpinBox;
	m_GameplayBlurSpinBox->setRange(0.0, 1.0);
	m_GameplayBlurSpinBox->setSingleStep(0.05);
	m_GameplayBlurSpinBox->setDecimals(2);
	m_GameplayBlurSpinBox->setToolTip("Blur when objects are visible (drain time).");

	m_BreakBlurSpinBox = new QDoubleSpinBox;
	m_BreakBlurSpinBox->setRange(0.0, 1.0);
	m_BreakBlurSpinBox->setSingleStep(0.05);
	m_BreakBlurSpinBox->setDecimals(2);
	m_BreakBlurSpinBox->setToolTip("Blur during breaks.");

	auto iBlur = new QLabel("Blur during intro");
	auto gBlur = new QLabel("Blur during gameplay");
	auto bBlur = new QLabel("Blur during breaks");
	iBlur->setToolTip(m_IntroBlurSpinBox->toolTip());
	gBlur->setToolTip(m_GameplayBlurSpinBox->toolTip());
	bBlur->setToolTip(m_BreakBlurSpinBox->toolTip());

	m_BlurLayout->addWidget(iBlur, 0, 0);
	m_BlurLayout->addWidget(gBlur, 1, 0);
	m_BlurLayout->addWidget(bBlur, 2, 0);
	m_BlurLayout->addWidget(m_IntroBlurSpinBox, 0, 1);
	m_BlurLayout->addWidget(m_GameplayBlurSpinBox, 1, 1);
	m_BlurLayout->addWidget(m_BreakBlurSpinBox, 2, 1);

	m_BlurGroupBox->setLayout(m_BlurLayout);
}

void BackgroundSettingsWidget::CreateTrianglesGroupBox()
{
	m_TrianglesGroupBox = new QGroupBox("Draw Background Triangles");
	m_TrianglesGroupBox->setCheckable(true);
	m_TrianglesGroupBox->setChecked(false);

	m_TrianglesLayout = new QGridLayout;

	m_TrianglesShadowed = new QCheckBox;
	m_TrianglesOverBlur = new QCheckBox;
	m_TrianglesShadowed->setToolTip("Enable/disable triangles have a slight shadow around them.");
	m_TrianglesOverBlur->setToolTip("Enable/disable triangles being drawn over blur (not blurred).");

	m_TrianglesParallaxMultiplier = new QDoubleSpinBox;
	m_TrianglesParallaxMultiplier->setRange(0.0, 10.0);
	m_TrianglesParallaxMultiplier->setSingleStep(0.05);
	m_TrianglesParallaxMultiplier->setDecimals(2);
	m_TrianglesParallaxMultiplier->setToolTip("Should triangles parallax less or more compared to background.");

	m_TrianglesDensity = new QDoubleSpinBox;
	m_TrianglesDensity->setRange(0.0, 10.0);
	m_TrianglesDensity->setSingleStep(0.05);
	m_TrianglesDensity->setDecimals(2);
	m_TrianglesDensity->setToolTip("How many triangles should be spawned compared to baseline.");

	m_TrianglesScale = new QDoubleSpinBox;
	m_TrianglesScale->setRange(0.0, 10.0);
	m_TrianglesScale->setSingleStep(0.05);
	m_TrianglesScale->setDecimals(2);
	m_TrianglesScale->setToolTip("How big triangles should be compared to baseline.");

	m_TrianglesSpeed = new QDoubleSpinBox;
	m_TrianglesSpeed->setRange(0.0, 10.0);
	m_TrianglesSpeed->setSingleStep(0.05);
	m_TrianglesSpeed->setDecimals(2);
	m_TrianglesSpeed->setToolTip("How fast triangles should move compared to baseline.");

	auto tShadow = new QLabel("Draw triangle shadow");
	auto tOverBlur = new QLabel("Draw triangles over blur (not blurred)");
	auto tParallax = new QLabel("Triangle parallax multiplier");
	auto tDensity = new QLabel("Triangle density");
	auto tScale = new QLabel("Triangle scale");
	auto tSpeed = new QLabel("Triangle speed");

	tShadow->setToolTip(m_TrianglesShadowed->toolTip());
	tOverBlur->setToolTip(m_TrianglesOverBlur->toolTip());
	tParallax->setToolTip(m_TrianglesParallaxMultiplier->toolTip());
	tDensity->setToolTip(m_TrianglesDensity->toolTip());
	tScale->setToolTip(m_TrianglesScale->toolTip());
	tSpeed->setToolTip(m_TrianglesSpeed->toolTip());

	m_TrianglesLayout->addWidget(tShadow, 0, 0);
	m_TrianglesLayout->addWidget(tOverBlur, 1, 0);
	m_TrianglesLayout->addWidget(tParallax, 2, 0);
	m_TrianglesLayout->addWidget(tDensity, 3, 0);
	m_TrianglesLayout->addWidget(tScale, 4, 0);
	m_TrianglesLayout->addWidget(tSpeed, 5, 0);

	m_TrianglesLayout->addWidget(m_TrianglesShadowed, 0, 1);
	m_TrianglesLayout->addWidget(m_TrianglesOverBlur, 1, 1);
	m_TrianglesLayout->addWidget(m_TrianglesParallaxMultiplier, 2, 1);
	m_TrianglesLayout->addWidget(m_TrianglesDensity, 3, 1);
	m_TrianglesLayout->addWidget(m_TrianglesScale, 4, 1);
	m_TrianglesLayout->addWidget(m_TrianglesSpeed, 5, 1);

	m_TrianglesGroupBox->setLayout(m_TrianglesLayout);
}

void BackgroundSettingsWidget::ReadSettings(const Settings& settings)
{
	// general
	m_LoadStoryboards->setChecked(settings.Playfield.Background.LoadStoryboards);
	m_LoadVideos->setChecked(settings.Playfield.Background.LoadVideos);
	m_FlashToTheBeat->setChecked(settings.Playfield.Background.FlashToTheBeat);

	// dim
	m_IntroDimSpinBox->setValue(settings.Playfield.Background.Dim.Intro);
	m_GameplayDimSpinBox->setValue(settings.Playfield.Background.Dim.Normal);
	m_BreakDimSpinBox->setValue(settings.Playfield.Background.Dim.Breaks);

	// parallax
	m_ParallaxAmount->setValue(settings.Playfield.Background.Parallax.Amount);
	m_ParallaxSpeed->setValue(settings.Playfield.Background.Parallax.Speed);

	// blur
	m_BlurGroupBox->setChecked(settings.Playfield.Background.Blur.Enabled);
	m_IntroBlurSpinBox->setValue(settings.Playfield.Background.Blur.Values.Intro);
	m_GameplayBlurSpinBox->setValue(settings.Playfield.Background.Blur.Values.Normal);
	m_BreakBlurSpinBox->setValue(settings.Playfield.Background.Blur.Values.Breaks);

	// triangles
	m_TrianglesGroupBox->setChecked(settings.Playfield.Background.Triangles.Enabled);
	m_TrianglesShadowed->setChecked(settings.Playfield.Background.Triangles.Shadowed);
	m_TrianglesOverBlur->setChecked(settings.Playfield.Background.Triangles.DrawOverBlur);

	m_TrianglesParallaxMultiplier->setValue(settings.Playfield.Background.Triangles.ParallaxMultiplier);
	m_TrianglesDensity->setValue(settings.Playfield.Background.Triangles.Density);
	m_TrianglesScale->setValue(settings.Playfield.Background.Triangles.Scale);
	m_TrianglesSpeed->setValue(settings.Playfield.Background.Triangles.Speed);
}

void BackgroundSettingsWidget::WriteSettings(Settings& settings)
{
	// general
	settings.Playfield.Background.LoadStoryboards = m_LoadStoryboards->isChecked();
	settings.Playfield.Background.LoadVideos = m_LoadVideos->isChecked();
	settings.Playfield.Background.FlashToTheBeat = m_FlashToTheBeat->isChecked();

	// dim
	settings.Playfield.Background.Dim.Intro = m_IntroDimSpinBox->value();
	settings.Playfield.Background.Dim.Normal = m_GameplayDimSpinBox->value();
	settings.Playfield.Background.Dim.Breaks = m_BreakDimSpinBox->value();

	// parallax
	settings.Playfield.Background.Parallax.Amount = m_ParallaxAmount->value();
	settings.Playfield.Background.Parallax.Speed = m_ParallaxSpeed->value();

	// blur
	settings.Playfield.Background.Blur.Enabled = m_BlurGroupBox->isChecked();

	settings.Playfield.Background.Blur.Values.Intro = m_IntroBlurSpinBox->value();
	settings.Playfield.Background.Blur.Values.Normal = m_GameplayBlurSpinBox->value();
	settings.Playfield.Background.Blur.Values.Breaks = m_GameplayBlurSpinBox->value();

	// triangles
	settings.Playfield.Background.Triangles.Enabled = m_TrianglesGroupBox->isChecked();
	settings.Playfield.Background.Triangles.Shadowed = m_TrianglesShadowed->isChecked();
	settings.Playfield.Background.Triangles.DrawOverBlur = m_TrianglesOverBlur->isChecked();

	settings.Playfield.Background.Triangles.ParallaxMultiplier = m_TrianglesParallaxMultiplier->value();
	settings.Playfield.Background.Triangles.Density = m_TrianglesDensity->value();
	settings.Playfield.Background.Triangles.Scale = m_TrianglesScale->value();
	settings.Playfield.Background.Triangles.Speed = m_TrianglesSpeed->value();
}

// SLOTS

void BackgroundSettingsWidget::IntroDimSliderChanged(int value)
{
	m_IntroDimSpinBox->setValue(value / 100.0);
}

void BackgroundSettingsWidget::GameplayDimSliderChanged(int value)
{
	m_GameplayDimSpinBox->setValue(value / 100.0);
}

void BackgroundSettingsWidget::BreakDimSliderChanged(int value)
{
	m_BreakDimSpinBox->setValue(value / 100.0);
}

void BackgroundSettingsWidget::IntroDimSpinBoxChanged(double value)
{
	m_IntroDimSlider->setValue(value * 100);
}

void BackgroundSettingsWidget::GameplayDimSpinBoxChanged(double value)
{
	m_GameplayDimSlider->setValue(value * 100);
}

void BackgroundSettingsWidget::BreakDimSpinBoxChanged(double value)
{
	m_BreakDimSlider->setValue(value * 100);
}
