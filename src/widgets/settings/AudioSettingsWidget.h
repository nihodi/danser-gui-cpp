#pragma once


#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>
#include <QJsonObject>

#include <QSlider>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QCheckBox>

#include "io/Settings.h"

class AudioSettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit AudioSettingsWidget(QWidget* parent = nullptr);

	void ReadSettings(const Settings& settings);
	void WriteSettings(Settings& settings) const;

private:
	QGridLayout* m_Layout;

	// VOLUME
	QGroupBox* m_VolumeGroupBox;
	QGridLayout* m_VolumeGroupBoxLayout;

	QSlider* m_MasterVolumeSlider;
	QSlider* m_MusicVolumeSlider;
	QSlider* m_EffectsVolumeSlider;
	QDoubleSpinBox* m_MasterVolumeSpinBox;
	QDoubleSpinBox* m_MusicVolumeSpinBox;
	QDoubleSpinBox* m_EffectsVolumeSpinBox;

	// OFFSET
	QSpinBox* m_OffsetSpinBox;

	// SAMPLES
	QGroupBox* m_SampleGroupBox;
	QGridLayout* m_SampleGridLayout;

	QCheckBox* m_IgnoreBeatmapSamples;
	QCheckBox* m_IgnoreBeatmapSamplesVolume;

private slots:
	void MasterVolmeSliderChanged(int value);
	void MusicVolmeSliderChanged(int value);
	void EffectsVolmeSliderChanged(int value);
	void MasterVolmeSpinBoxChanged(double value);
	void MusicVolmeSpinBoxChanged(double value);
	void EffectsVolmeSpinBoxChanged(double value);
};
