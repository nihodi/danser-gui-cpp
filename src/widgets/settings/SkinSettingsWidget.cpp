#include "SkinSettingsWidget.h"

#include <filesystem>
#include <iostream>

SkinSettingsWidget::SkinSettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout(this);

	CreateGeneralGroupBox();
	CreateCursorGroupBox();
	m_Layout->addWidget(m_GeneralGroupBox, 0, 0);
	m_Layout->addWidget(m_CursorGroupBox, 1, 0);

	setMaximumHeight(m_GeneralGroupBox->maximumHeight() + m_CursorGroupBox->maximumHeight());
}

void SkinSettingsWidget::CreateGeneralGroupBox()
{
	m_GeneralLayout = new QGridLayout;

	m_GeneralGroupBox = new QGroupBox("General");
	m_GeneralGroupBox->setLayout(m_GeneralLayout);
	m_GeneralGroupBox->setMaximumHeight(200);

	m_SkinComboBox = new QComboBox;
	m_FallbackComboBox = new QComboBox;
	m_FallbackComboBox->setToolTip("Loads this skin if the above skin fails to load."
								   "\nOtherwise it loads assets from it if CurrentSkin misses them.");

	m_UseColorsFromSkin = new QCheckBox;
	m_UseBeatmapColors = new QCheckBox;

	m_UseColorsFromSkin->setToolTip("Determines if colors from skin.ini should be used.");
	m_UseBeatmapColors->setToolTip("Determines if colors from skin.ini should be used.");

	auto fallback = new QLabel("Fallback skin");
	auto sColors = new QLabel("Use skin colors");
	auto bColors = new QLabel("Use beatmap colors");
	fallback->setToolTip(m_FallbackComboBox->toolTip());
	sColors->setToolTip(m_UseColorsFromSkin->toolTip());
	bColors->setToolTip(m_UseBeatmapColors->toolTip());

	m_GeneralLayout->addWidget(new QLabel("Skin"), 0, 0);
	m_GeneralLayout->addWidget(fallback, 1, 0);
	m_GeneralLayout->addWidget(sColors, 2, 0);
	m_GeneralLayout->addWidget(bColors, 3, 0);

	m_GeneralLayout->addWidget(m_SkinComboBox, 0, 1);
	m_GeneralLayout->addWidget(m_FallbackComboBox, 1, 1);
	m_GeneralLayout->addWidget(m_UseColorsFromSkin, 2, 1);
	m_GeneralLayout->addWidget(m_UseBeatmapColors, 3, 1);

	connect(m_UseColorsFromSkin, SIGNAL(clicked(bool)), m_UseBeatmapColors, SLOT(setDisabled(bool)));
	connect(m_UseBeatmapColors, SIGNAL(clicked(bool)), m_UseColorsFromSkin, SLOT(setDisabled(bool)));
}

void SkinSettingsWidget::CreateCursorGroupBox()
{
	m_CursorLayout = new QGridLayout;
	m_CursorGroupBox = new QGroupBox("Cursor");
	m_CursorGroupBox->setLayout(m_CursorLayout);
	m_CursorGroupBox->setMaximumHeight(300);
	// TODO: setmaximumheight

	m_UseSkinCursor = new QCheckBox;
	m_ForceLongTrail = new QCheckBox;

	m_UseSkinCursor->setToolTip("Use cursor from the skin, instead of the Danser cursor.");
	m_ForceLongTrail->setToolTip("Forces long trail for any skin cursor (simulates cursormiddle.png presence).");

	m_LongTrailLength = new QSpinBox;
	m_LongTrailDensity = new QDoubleSpinBox;
	m_LongTrailLength->setRange(0, 1000000000);

	m_CursorScale = new QDoubleSpinBox;
	m_CursorTrailScale = new QDoubleSpinBox;

	m_CursorScale->setDecimals(2);
	m_CursorScale->setRange(0.01, 1000);
	m_CursorScale->setSingleStep(0.1);
	m_CursorTrailScale->setDecimals(2);
	m_CursorTrailScale->setRange(0.01, 1000);
	m_CursorTrailScale->setSingleStep(0.1);

	auto sCursor = new QLabel("Use the skin's cursor");
	auto forceLongTrail = new QLabel("Cursor scale");
	sCursor->setToolTip(m_UseSkinCursor->toolTip());
	forceLongTrail->setToolTip(m_ForceLongTrail->toolTip());

	m_CursorLayout->addWidget(sCursor, 0, 0);
	m_CursorLayout->addWidget(forceLongTrail, 1, 0);
	m_CursorLayout->addWidget(new QLabel("Cursor trail scale"), 2, 0);
	m_CursorLayout->addWidget(new QLabel("Force long trail"), 3, 0);
	m_CursorLayout->addWidget(new QLabel("Long trail length"), 4, 0);
	m_CursorLayout->addWidget(new QLabel("Long trail density"), 5, 0);

	m_CursorLayout->addWidget(m_UseSkinCursor, 0, 1);
	m_CursorLayout->addWidget(m_CursorScale, 1, 1);
	m_CursorLayout->addWidget(m_CursorTrailScale, 2, 1);
	m_CursorLayout->addWidget(m_ForceLongTrail, 3, 1);
	m_CursorLayout->addWidget(m_LongTrailLength, 4, 1);
	m_CursorLayout->addWidget(m_LongTrailDensity, 5, 1);
}

void SkinSettingsWidget::ReadSettings(const Settings& settings)
{

	// find skins and add them to the relevant combo boxes
	FindSkins(settings);

	m_UseColorsFromSkin->setChecked(settings.Skin.UseColorsFromSkin);
	m_UseBeatmapColors->setChecked(settings.Skin.UseBeatmapColors);

	// if both UseColorsFromSKin and UseBeatmapColors are true
	// UseColorsFromSkin gets disabled
	if (settings.Skin.UseColorsFromSkin && settings.Skin.UseBeatmapColors)
		m_UseColorsFromSkin->setChecked(false);

	m_UseColorsFromSkin->setDisabled(m_UseBeatmapColors->isChecked());
	m_UseBeatmapColors->setDisabled(m_UseColorsFromSkin->isChecked());

	// Cursor things
	m_UseSkinCursor->setChecked(settings.Skin.Cursor.UseSkinCursor);
	m_CursorScale->setValue(settings.Skin.Cursor.Scale);
	m_CursorTrailScale->setValue(settings.Skin.Cursor.TrailScale);
	m_ForceLongTrail->setChecked(settings.Skin.Cursor.ForceLongTrail);
	m_LongTrailLength->setValue(settings.Skin.Cursor.LongTrailLength);
	m_LongTrailDensity->setValue(settings.Skin.Cursor.LongTrailDensity);
}

void SkinSettingsWidget::WriteSettings(Settings& settings) const
{
	// general
	settings.Skin.CurrentSkin = m_SkinComboBox->currentText();
	settings.Skin.FallbackSkin = m_FallbackComboBox->currentText();

	settings.Skin.UseColorsFromSkin = m_UseColorsFromSkin->isChecked();
	settings.Skin.UseBeatmapColors = m_UseBeatmapColors->isChecked();

	// cursor
	settings.Skin.Cursor.UseSkinCursor = m_UseSkinCursor->isChecked();
	settings.Skin.Cursor.Scale = m_CursorScale->value();
	settings.Skin.Cursor.TrailScale = m_CursorTrailScale->value();
	settings.Skin.Cursor.ForceLongTrail = m_ForceLongTrail->isChecked();
	settings.Skin.Cursor.LongTrailLength = m_LongTrailLength->value();
	settings.Skin.Cursor.LongTrailDensity = m_LongTrailDensity->value();
}

void SkinSettingsWidget::FindSkins(const Settings& settings)
{
	m_SkinComboBox->clear();
	m_FallbackComboBox->clear();
	m_SkinComboBox->addItem("default");
	m_FallbackComboBox->addItem("default");

	const std::filesystem::path skinPath(settings.General.OsuSkinsDir.toStdString());
	// iterate over the skin path to find all skin folder names
	// TODO: Make this update every time the OsuSkinsDir is updated
	for (const auto& dir : std::filesystem::directory_iterator(skinPath))
	{
		m_SkinComboBox->addItem(QString(dir.path().filename().string().c_str()));
		m_FallbackComboBox->addItem(QString(dir.path().filename().string().c_str()));
	}

	// select the relevant skin by iterating through the QComboBox's items and checking if the strings match
	for (int i = 0; i < m_SkinComboBox->count(); i++)
	{
		if (m_SkinComboBox->itemText(i) == settings.Skin.CurrentSkin)
		{
			m_SkinComboBox->setCurrentIndex(i);
			std::cout << "Found skin: " << m_SkinComboBox->itemText(i).toStdString() << std::endl;
		}

		if (m_FallbackComboBox->itemText(i) == settings.Skin.FallbackSkin)
		{
			m_FallbackComboBox->setCurrentIndex(i);
			std::cout << "Found fallback skin: " << m_FallbackComboBox->itemText(i).toStdString() << std::endl;
		}
	}
}
