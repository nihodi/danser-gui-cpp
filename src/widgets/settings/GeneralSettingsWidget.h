#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>
#include <QJsonObject>

#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QCheckBox>

#include "io/Settings.h"

class GeneralSettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit  GeneralSettingsWidget(QWidget* parent = nullptr);

	void ReadSettings(const Settings& settings);
	void WriteSettings(Settings& settings) const;

private slots:
	void BrowsSongsDirectory();
	void BrowseSkinsDirectory();

private:
	void CreateOsuDirectoriesGroupBox();
	void CreateMiscGroupBox();

	QGridLayout* m_Layout;

	// osu! directories selection
	QGroupBox* m_OsuDirectoriesGroupBox;
	QGridLayout* m_OsuDirectoriesLayout;

	QLineEdit* m_OsuSongsDirectory;
	QLineEdit* m_OsuSkinsDirectory;
	QPushButton* m_BrowseOsuSongsDirectory;
	QPushButton* m_BrowseOsuSkinsDirectory;

	// Misc.
	// TODO: Refactor settings handling and add Epilepsy Warning and Danser Logo options here
	QGroupBox* m_MiscGroupBox;
	QGridLayout* m_MiscLayout;

	QCheckBox* m_DiscordRichPresence;

};
