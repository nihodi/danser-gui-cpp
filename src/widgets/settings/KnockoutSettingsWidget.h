#pragma once

#include "io/Settings.h"

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>

#include <QComboBox>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>

class KnockoutSettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit KnockoutSettingsWidget(QWidget* parent = nullptr);

	void ReadSettings(const Settings& settings);
	void WriteSettings(Settings& settings) const;

private:
	void CreateGroupBox();

	QGridLayout* m_Layout;

	QGroupBox* m_GroupBox;
	QGridLayout* m_GroupLayout;

	QComboBox* m_Mode;
	QComboBox* m_SortBy;

	QDoubleSpinBox* m_GraceEndTime;
	QSpinBox* m_BubbleMinimumCombo;
	QSpinBox* m_MaxPlayers;
	QCheckBox* m_RevivePlayersAtEnd;
	QCheckBox* m_LiveSort;
	QCheckBox* m_HideOverlayOnBreaks;
	QDoubleSpinBox* m_MinCursorSize;
	QDoubleSpinBox* m_MaxCursorSize;
	QCheckBox* m_AddDanser;
	QLineEdit* m_DanserName;

};
