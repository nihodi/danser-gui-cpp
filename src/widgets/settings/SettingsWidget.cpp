#include "SettingsWidget.h"

#include "platform.h"

#include <iostream>
#include <QMessageBox>

SettingsWidget::SettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	m_TabWidget = new QTabWidget;
	m_Layout = new QGridLayout;

	m_SaveSettingsButton = new QPushButton("Save settings!");


#ifdef linux
	m_SettingsFilePath = HOME;
	m_SettingsFilePath += "/.config/danser/danser-gui-cpp.json";

#else
	m_SettingsFilePath = "settings/danser-gui-cpp.json";
#endif

	m_GraphicsSettingsWidget = new GraphicsSettingsWidget(nullptr);
	m_AudioSettingsWidget = new AudioSettingsWidget(nullptr);
	m_BackgroundSettingsWidget = new BackgroundSettingsWidget(nullptr);
	m_GeneralSettingsWidget = new GeneralSettingsWidget(nullptr);
	m_CursorSettingsWidget = new CursorSettingsWidget(nullptr);
	m_SkinSettingsWidget = new SkinSettingsWidget(nullptr);
	m_KnockoutSettingsWidget = new KnockoutSettingsWidget(nullptr);

	m_TabWidget->addTab(m_GeneralSettingsWidget, "General");
	m_TabWidget->addTab(m_GraphicsSettingsWidget, "Graphics");
	m_TabWidget->addTab(m_AudioSettingsWidget, "Audio");
	m_TabWidget->addTab(m_BackgroundSettingsWidget, "Background");
	m_TabWidget->addTab(m_CursorSettingsWidget, "Cursor");
	m_TabWidget->addTab(m_SkinSettingsWidget, "Skin");
	m_TabWidget->addTab(m_KnockoutSettingsWidget, "Knockout");

	m_Layout->addWidget(m_TabWidget, 0, 0);
	m_Layout->addWidget(m_SaveSettingsButton, 1, 0);
	setLayout(m_Layout);

	connect(m_SaveSettingsButton, SIGNAL(pressed()), this, SLOT(SaveSettingsButtonPressed()));
	connect(m_TabWidget, SIGNAL(currentChanged(int)), this, SLOT(TabChanged(int)));
	TabChanged(0); // to update this->maximumHeight() to place save settings button in a meaningful place

	readSettingsfile(m_SettingsFilePath);
}

void SettingsWidget::readSettingsfile(const std::string& filepath)
{
	QFile file(filepath.c_str());

	if (!file.open(QIODevice::ReadOnly))
	{
#ifdef linux
		std::cout << "Linux: Could not open file " << filepath << ". Trying settings/danser-gui-cpp.json ..." << std::endl;
		readSettingsfile("settings/danser-gui-cpp.json");
		return;
#endif
		std::cout << "Could not open file " << filepath << std::endl;
		return;
	}

	std::cout << "Opened file " << filepath << std::endl;
	QByteArray jsonData = file.readAll();


	QJsonDocument document(QJsonDocument::fromJson(jsonData));


	std::cout << "Opened settings:\n" << document.toJson().toStdString() << std::endl;

	Settings::CurrentSettings.ReadJSON(document.object());
	m_GeneralSettingsWidget->ReadSettings(Settings::CurrentSettings);
	m_GraphicsSettingsWidget->ReadSettings(Settings::CurrentSettings);
	m_AudioSettingsWidget->ReadSettings(Settings::CurrentSettings);
	m_BackgroundSettingsWidget->ReadSettings(Settings::CurrentSettings);
	m_CursorSettingsWidget->ReadSettings(Settings::CurrentSettings);
	m_SkinSettingsWidget->ReadSettings(Settings::CurrentSettings);
	m_KnockoutSettingsWidget->ReadSettings(Settings::CurrentSettings);
}

void SettingsWidget::writeSettingsFile(const std::string& filepath)
{
	QFile file(filepath.c_str());

	std::cout << "Updating JSON document" << std::endl;

	m_GeneralSettingsWidget->WriteSettings(Settings::CurrentSettings);
	m_GraphicsSettingsWidget->WriteSettings(Settings::CurrentSettings);
	m_AudioSettingsWidget->WriteSettings(Settings::CurrentSettings);
	m_BackgroundSettingsWidget->WriteSettings(Settings::CurrentSettings);
	m_CursorSettingsWidget->WriteSettings(Settings::CurrentSettings);
	m_SkinSettingsWidget->WriteSettings(Settings::CurrentSettings);
	m_KnockoutSettingsWidget->WriteSettings(Settings::CurrentSettings);

	QJsonObject settings;
	Settings::CurrentSettings.WriteJSON(settings);

	QJsonDocument document(settings);

	if (!file.open(QIODevice::WriteOnly))
	{
		std::cout << "Could not open file for write: " << filepath << std::endl;
		return;
	}
	std::cout << document.toJson().toStdString();
	file.write(document.toJson());

	std::cout << "Writing to file " << filepath << std::endl;
}

void SettingsWidget::SaveSettingsButtonPressed()
{
	writeSettingsFile(m_SettingsFilePath);
}

void SettingsWidget::TabChanged(int index)
{
	setMaximumHeight(m_TabWidget->widget(index)->maximumHeight() + 50);
}
