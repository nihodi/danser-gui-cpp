#include "GeneralSettingsWidget.h"
#include "platform.h"

#include <QFileDialog>

GeneralSettingsWidget::GeneralSettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout;

	CreateOsuDirectoriesGroupBox();
	CreateMiscGroupBox();

	m_Layout->addWidget(m_OsuDirectoriesGroupBox, 0, 0);
	m_Layout->addWidget(m_MiscGroupBox, 1, 0);

	setLayout(m_Layout);
	setMaximumHeight(m_MiscGroupBox->maximumHeight() + m_OsuDirectoriesGroupBox->maximumHeight());
}

void GeneralSettingsWidget::CreateOsuDirectoriesGroupBox()
{
	m_OsuDirectoriesGroupBox = new QGroupBox("osu! folders");
	m_OsuDirectoriesLayout = new QGridLayout;

	m_OsuSongsDirectory = new QLineEdit;
	m_OsuSongsDirectory->setPlaceholderText("Path to osu! songs folder");
	m_OsuSongsDirectory->setToolTip("Set this to the osu! songs folder");

	m_OsuSkinsDirectory = new QLineEdit;
	m_OsuSkinsDirectory->setPlaceholderText("Path to osu! skins folder");
	m_OsuSkinsDirectory->setToolTip("Set this to the osu! skins folder");

	m_BrowseOsuSongsDirectory = new QPushButton("Browse");
	m_BrowseOsuSkinsDirectory = new QPushButton("Browse");

	auto songLabel = new QLabel("osu! songs folder");
	songLabel->setToolTip("Set this to the osu! songs folder");

	auto skinLabel = new QLabel("osu! skins folder");
	skinLabel->setToolTip("Set this to the osu! skins folder");

	m_OsuDirectoriesLayout->addWidget(songLabel, 0, 0);
	m_OsuDirectoriesLayout->addWidget(skinLabel, 1, 0);
	m_OsuDirectoriesLayout->addWidget(m_OsuSongsDirectory, 0, 1);
	m_OsuDirectoriesLayout->addWidget(m_OsuSkinsDirectory, 1, 1);
	m_OsuDirectoriesLayout->addWidget(m_BrowseOsuSongsDirectory, 0, 2);
	m_OsuDirectoriesLayout->addWidget(m_BrowseOsuSkinsDirectory, 1, 2);

	m_OsuDirectoriesGroupBox->setLayout(m_OsuDirectoriesLayout);
	m_OsuDirectoriesGroupBox->setMaximumHeight(150);

	connect(m_BrowseOsuSongsDirectory, SIGNAL(pressed()), this, SLOT(BrowsSongsDirectory()));
	connect(m_BrowseOsuSkinsDirectory, SIGNAL(pressed()), this, SLOT(BrowseSkinsDirectory()));
}

void GeneralSettingsWidget::BrowsSongsDirectory()
{
	QString directory = QFileDialog::getExistingDirectory(this, "Select osu! songs directory", HOME);
	if (directory.isEmpty())
		return;
	m_OsuSongsDirectory->setText(directory);
}

void GeneralSettingsWidget::BrowseSkinsDirectory()
{
	QString directory = QFileDialog::getExistingDirectory(this, "Select osu! skins directory", HOME);
	if (directory.isEmpty())
		return;
	m_OsuSkinsDirectory->setText(directory);
}

void GeneralSettingsWidget::CreateMiscGroupBox()
{
	m_MiscGroupBox = new QGroupBox("Misc.");
	m_MiscLayout = new QGridLayout;

	m_DiscordRichPresence = new QCheckBox;
	m_DiscordRichPresence->setToolTip("Enable or disable Discord rich presence");

	auto discordLabel = new QLabel("Discord rich presence");
	discordLabel->setToolTip("Enable or disable Discord rich presence");

	m_MiscLayout->addWidget(discordLabel, 0, 0);
	m_MiscLayout->addWidget(m_DiscordRichPresence, 0, 1);

	m_MiscGroupBox->setLayout(m_MiscLayout);
	m_MiscGroupBox->setMaximumHeight(80);
}

void GeneralSettingsWidget::ReadSettings(const Settings& settings)
{
	m_OsuSongsDirectory->setText(settings.General.OsuSongsDir);
	m_OsuSkinsDirectory->setText(settings.General.OsuSkinsDir);

	m_DiscordRichPresence->setChecked(settings.General.DiscordPresenceOn);
}

void GeneralSettingsWidget::WriteSettings(Settings& settings) const
{
	settings.General.OsuSongsDir = m_OsuSongsDirectory->text();
	settings.General.OsuSkinsDir = m_OsuSkinsDirectory->text();

	settings.General.DiscordPresenceOn = m_DiscordRichPresence->isChecked();
}
