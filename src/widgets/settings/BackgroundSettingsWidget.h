#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>

#include <QLabel>
#include <QSlider>
#include <QDoubleSpinBox>
#include <QCheckBox>

#include <QJsonObject>

#include "io/Settings.h"

class BackgroundSettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit BackgroundSettingsWidget(QWidget* parent = nullptr);

	void ReadSettings(const Settings& settings);
	void WriteSettings(Settings& settings);

private slots:
	void IntroDimSliderChanged(int value);
	void GameplayDimSliderChanged(int value);
	void BreakDimSliderChanged(int value);

	void IntroDimSpinBoxChanged(double value);
	void GameplayDimSpinBoxChanged(double value);
	void BreakDimSpinBoxChanged(double value);

private:
	void CreateDimGroupBox();
	void CreateParallaxGroupBox();
	void CreateBlurGroupBox();
	void CreateTrianglesGroupBox();

	QGridLayout* m_Layout;

	// GENERAL
	QCheckBox* m_LoadStoryboards;
	QCheckBox* m_LoadVideos;
	QCheckBox* m_FlashToTheBeat;

	// DIM
	QGridLayout* m_DimLayout;
	QGroupBox* m_DimGroupBox;

	QSlider* m_IntroDimSlider;
	QSlider* m_GameplayDimSlider;
	QSlider* m_BreakDimSlider;

	QDoubleSpinBox* m_IntroDimSpinBox;
	QDoubleSpinBox* m_GameplayDimSpinBox;
	QDoubleSpinBox* m_BreakDimSpinBox;

	// Parallax
	QGridLayout* m_ParallaxLayout;
	QGroupBox* m_ParallaxGroupBox;

	QDoubleSpinBox* m_ParallaxAmount;
	QDoubleSpinBox* m_ParallaxSpeed;

	// BLUR
	QGridLayout* m_BlurLayout;
	QGroupBox* m_BlurGroupBox;

	QDoubleSpinBox* m_IntroBlurSpinBox;
	QDoubleSpinBox* m_GameplayBlurSpinBox;
	QDoubleSpinBox* m_BreakBlurSpinBox;

	// TRIANGLES
	QGridLayout* m_TrianglesLayout;
	QGroupBox* m_TrianglesGroupBox;

	QCheckBox* m_TrianglesShadowed;
	QCheckBox* m_TrianglesOverBlur;
	QDoubleSpinBox* m_TrianglesParallaxMultiplier;

	QDoubleSpinBox* m_TrianglesDensity;
	QDoubleSpinBox* m_TrianglesScale;
	QDoubleSpinBox* m_TrianglesSpeed;
};
