#include "AudioSettingsWidget.h"

#include <QLabel>

AudioSettingsWidget::AudioSettingsWidget(QWidget* parent)
	: QWidget(parent)
{
	m_Layout = new QGridLayout;

	m_VolumeGroupBox = new QGroupBox("Volume controls");
	m_VolumeGroupBoxLayout = new QGridLayout;

	// VOLUME THINGS
	// SLIDERS
	m_MasterVolumeSlider = new QSlider(Qt::Horizontal);
	m_MasterVolumeSlider->setRange(0, 100);
	m_MasterVolumeSlider->setSingleStep(1);
	m_MasterVolumeSlider->setToolTip("Change the master volume");

	m_MusicVolumeSlider = new QSlider(Qt::Horizontal);
	m_MusicVolumeSlider->setRange(0, 100);
	m_MusicVolumeSlider->setSingleStep(1);
	m_MusicVolumeSlider->setToolTip("Change the music volume");

	m_EffectsVolumeSlider = new QSlider(Qt::Horizontal);
	m_EffectsVolumeSlider->setRange(0, 100);
	m_EffectsVolumeSlider->setSingleStep(1);
	m_EffectsVolumeSlider->setToolTip("Change the effects (hitsounds) volume");

	// SPIN BOXES
	m_MasterVolumeSpinBox = new QDoubleSpinBox;
	m_MasterVolumeSpinBox->setRange(0.0, 1.0);
	m_MasterVolumeSpinBox->setSingleStep(0.01);
	m_MasterVolumeSpinBox->setToolTip(m_MasterVolumeSlider->toolTip());

	m_MusicVolumeSpinBox = new QDoubleSpinBox;
	m_MusicVolumeSpinBox->setRange(0.0, 1.0);
	m_MusicVolumeSpinBox->setSingleStep(0.01);
	m_MusicVolumeSpinBox->setToolTip(m_MusicVolumeSlider->toolTip());

	m_EffectsVolumeSpinBox = new QDoubleSpinBox;
	m_EffectsVolumeSpinBox->setRange(0.0, 1.0);
	m_EffectsVolumeSpinBox->setSingleStep(0.01);
	m_EffectsVolumeSpinBox->setToolTip(m_EffectsVolumeSlider->toolTip());

	auto masterLabel = new QLabel("Master volume");
	auto musicLabel = new QLabel("Music volume");
	auto effectsLabel = new QLabel("Effects volume");

	masterLabel->setToolTip(m_MasterVolumeSlider->toolTip());
	musicLabel->setToolTip(m_MusicVolumeSlider->toolTip());
	effectsLabel->setToolTip(m_EffectsVolumeSlider->toolTip());

	m_VolumeGroupBoxLayout->addWidget(masterLabel, 0, 0);
	m_VolumeGroupBoxLayout->addWidget(musicLabel, 1, 0);
	m_VolumeGroupBoxLayout->addWidget(effectsLabel, 2, 0);

	m_VolumeGroupBoxLayout->addWidget(m_MasterVolumeSlider, 0, 1);
	m_VolumeGroupBoxLayout->addWidget(m_MusicVolumeSlider, 1, 1);
	m_VolumeGroupBoxLayout->addWidget(m_EffectsVolumeSlider, 2, 1);

	m_VolumeGroupBoxLayout->addWidget(m_MasterVolumeSpinBox, 0, 2);
	m_VolumeGroupBoxLayout->addWidget(m_MusicVolumeSpinBox, 1, 2);
	m_VolumeGroupBoxLayout->addWidget(m_EffectsVolumeSpinBox, 2, 2);

	m_VolumeGroupBox->setLayout(m_VolumeGroupBoxLayout);
	m_VolumeGroupBox->setMaximumHeight(180);

	// OFFSET
	m_OffsetSpinBox = new QSpinBox;
	m_OffsetSpinBox->setRange(-1000, 1000);
	m_OffsetSpinBox->setSuffix("ms");
	m_OffsetSpinBox->setSingleStep(1);
	m_OffsetSpinBox->setToolTip("Set the offset. This is the same as Universal Offset in osu!");

	// SAMPLES
	m_SampleGridLayout = new QGridLayout;
	m_SampleGroupBox = new QGroupBox("Beatmap Samples");

	m_IgnoreBeatmapSamples = new QCheckBox;
	m_IgnoreBeatmapSamplesVolume = new QCheckBox;

	m_IgnoreBeatmapSamples->setToolTip("Ignore the beatmap's samples and use the skins samples instead.");
	m_IgnoreBeatmapSamplesVolume->setToolTip("Ignore the beatmap's sample volumes and play every sample at the same loudness.");

	auto ignoreSamples = new QLabel("Ignore Beatmap hit samples (hitsounds)");
	auto ignoreVolumes = new QLabel("Ignore Beatmap hit sample (hitsounds) volumes");
	ignoreSamples->setToolTip(m_IgnoreBeatmapSamples->toolTip());
	ignoreVolumes->setToolTip(m_IgnoreBeatmapSamplesVolume->toolTip());

	m_SampleGridLayout->addWidget(ignoreSamples, 0, 0);
	m_SampleGridLayout->addWidget(ignoreVolumes, 1, 0);
	m_SampleGridLayout->addWidget(m_IgnoreBeatmapSamples, 0, 1);
	m_SampleGridLayout->addWidget(m_IgnoreBeatmapSamplesVolume, 1, 1);
	m_SampleGroupBox->setMaximumHeight(120);

	m_SampleGroupBox->setLayout(m_SampleGridLayout);

	// add things to layout
	auto offsetLabel = new QLabel("Offset");
	offsetLabel->setToolTip(m_OffsetSpinBox->toolTip());

	m_Layout->addWidget(offsetLabel, 0, 0);
	m_Layout->addWidget(m_OffsetSpinBox, 0, 1);
	m_Layout->addWidget(m_VolumeGroupBox, 1, 0, 1, 2);
	m_Layout->addWidget(m_SampleGroupBox, 2, 0, 1, 2);
	setLayout(m_Layout);

	connect(m_MasterVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(MasterVolmeSliderChanged(int)));
	connect(m_MusicVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(MusicVolmeSliderChanged(int)));
	connect(m_EffectsVolumeSlider, SIGNAL(valueChanged(int)), this, SLOT(EffectsVolmeSliderChanged(int)));

	connect(m_MasterVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(MasterVolmeSpinBoxChanged(double)));
	connect(m_MusicVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(MusicVolmeSpinBoxChanged(double)));
	connect(m_EffectsVolumeSpinBox, SIGNAL(valueChanged(double)), this, SLOT(EffectsVolmeSpinBoxChanged(double)));

	setMaximumHeight(400);
}

// SLIDER CHANGED
void AudioSettingsWidget::MasterVolmeSliderChanged(int value)
{
	m_MasterVolumeSpinBox->setValue(value/100.0);
}


void AudioSettingsWidget::MusicVolmeSliderChanged(int value)
{
	m_MusicVolumeSpinBox->setValue(value/100.0);
}

void AudioSettingsWidget::EffectsVolmeSliderChanged(int value)
{
	m_EffectsVolumeSpinBox->setValue(value/100.0);

}

void AudioSettingsWidget::MasterVolmeSpinBoxChanged(double value)
{
	m_MasterVolumeSlider->setValue(value * 100);
}



void AudioSettingsWidget::MusicVolmeSpinBoxChanged(double value)
{
	m_MusicVolumeSlider->setValue(value * 100);
}

void AudioSettingsWidget::EffectsVolmeSpinBoxChanged(double value)
{
	m_EffectsVolumeSlider->setValue(value * 100);
}

void AudioSettingsWidget::ReadSettings(const Settings& settings)
{
	m_MasterVolumeSpinBox->setValue(settings.Audio.GeneralVolume);
	m_MusicVolumeSpinBox->setValue(settings.Audio.MusicVolume);
	m_EffectsVolumeSpinBox->setValue(settings.Audio.SampleVolume);
	m_OffsetSpinBox->setValue(settings.Audio.Offset);

	m_IgnoreBeatmapSamples->setChecked(settings.Audio.IgnoreBeatmapSamples);
	m_IgnoreBeatmapSamplesVolume->setChecked(settings.Audio.IgnoreBeatmapSampleVolume);
}

void AudioSettingsWidget::WriteSettings(Settings& settings) const
{
	settings.Audio.GeneralVolume = m_MasterVolumeSpinBox->value();
	settings.Audio.MusicVolume = m_MusicVolumeSpinBox->value();
	settings.Audio.SampleVolume = m_EffectsVolumeSpinBox->value();

	settings.Audio.Offset = m_OffsetSpinBox->value();
	settings.Audio.IgnoreBeatmapSamples = m_IgnoreBeatmapSamples->isChecked();
	settings.Audio.IgnoreBeatmapSampleVolume = m_IgnoreBeatmapSamplesVolume->isChecked();
}
