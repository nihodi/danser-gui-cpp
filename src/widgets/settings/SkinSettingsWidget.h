#pragma once

#include "io/Settings.h"

#include <QWidget>
#include <QGridLayout>
#include <QGroupBox>

#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QSpinBox>

class SkinSettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit SkinSettingsWidget(QWidget* parent = nullptr);

	void ReadSettings(const Settings& settings);
	void WriteSettings(Settings& settings) const;


private:
	void CreateGeneralGroupBox();
	void CreateCursorGroupBox();

	void FindSkins(const Settings& settings);

	QGridLayout* m_Layout;

	// general
	QGroupBox* m_GeneralGroupBox;
	QGridLayout* m_GeneralLayout;

	QComboBox* m_SkinComboBox;
	QComboBox* m_FallbackComboBox;
	QCheckBox* m_UseColorsFromSkin;
	QCheckBox* m_UseBeatmapColors;

	// cursor
	QGroupBox* m_CursorGroupBox;
	QGridLayout* m_CursorLayout;

	QCheckBox* m_UseSkinCursor;
	QDoubleSpinBox* m_CursorScale;
	QDoubleSpinBox* m_CursorTrailScale;
	QCheckBox* m_ForceLongTrail;
	QSpinBox* m_LongTrailLength;
	QDoubleSpinBox* m_LongTrailDensity;
};
