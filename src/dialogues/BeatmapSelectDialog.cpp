#include "BeatmapSelectDialog.h"

#include "io/BeatmapManager.h"
#include "io/Settings.h"
#include "beatmap/BeatmapListWidgetItem.h"

#include "platform.h"

BeatmapSelectDialog::BeatmapSelectDialog(QWidget* parent)
		: QDialog(parent)
{
	m_Layout = new QVBoxLayout;

	m_GroupLayout = new QGridLayout;
	m_GroupBox = new QGroupBox("Search for beatmap");

	m_TitleLabel = new QLabel("Song Title");
	m_ArtistLabel = new QLabel("Artist");
	m_DiffnameLabel = new QLabel("Diffname");
	m_MapperLabel = new QLabel("Mapper");

	m_TitleInput = new QLineEdit;
	m_ArtistInput = new QLineEdit;
	m_DiffNameInput = new QLineEdit;
	m_MapperInput = new QLineEdit;

	m_TitleInput->setPlaceholderText("Song Title");
	m_ArtistInput->setPlaceholderText("Artist");
	m_DiffNameInput->setPlaceholderText("Diffname");
	m_MapperInput->setPlaceholderText("Mapper");


	m_BeatmapResults = new QListWidget;
	m_BeatmapResults->setSelectionMode(QAbstractItemView::SingleSelection);

	m_ReloadBeatmapsFromDisk = new QPushButton("Reload beatmaps from disk");
	connect(m_ReloadBeatmapsFromDisk, SIGNAL(pressed()), this, SLOT(ReloadBeatmapsFromDisk()));

	m_SelectButton = new QPushButton("Select");
	connect(m_SelectButton, SIGNAL(pressed()), this, SLOT(close()));

		m_SearchLayout = new QGridLayout;
		m_SearchGroupBox = new QGroupBox;
		m_SearchGroupBox->setLayout(m_SearchLayout);

		m_SearchLayout->addWidget(m_TitleLabel, 0, 0);
		m_SearchLayout->addWidget(m_TitleInput, 0, 1);

		m_SearchLayout->addWidget(m_ArtistLabel, 1, 0);
		m_SearchLayout->addWidget(m_ArtistInput, 1, 1);
		m_SearchLayout->addWidget(m_DiffnameLabel, 2, 0);
		m_SearchLayout->addWidget(m_DiffNameInput, 2, 1);
		m_SearchLayout->addWidget(m_MapperLabel, 3, 0);
		m_SearchLayout->addWidget(m_MapperInput, 3, 1);

	m_GroupLayout->addWidget(m_SearchGroupBox, 0, 0);

	/*
	m_BeatmapResults->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

	m_ArtistInput->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	m_DiffNameInput->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	m_MapperInput->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	m_TitleInput->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	m_TitleLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
	m_ArtistLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
	m_DiffnameLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
	m_MapperLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
	*/

	m_GroupLayout->addWidget(m_ReloadBeatmapsFromDisk, 1, 0);
	m_GroupLayout->addWidget(m_BeatmapResults, 0, 1);

	m_GroupLayout->addWidget(m_SelectButton, 1, 1);

	m_ImagePreview = new QLabel;
	m_GroupLayout->addWidget(m_ImagePreview, 7, 0, 1, 4, Qt::AlignCenter);

	m_GroupBox->setLayout(m_GroupLayout);
	m_GroupBox->setMaximumHeight(700);

	m_Layout->addWidget(m_GroupBox);
	setLayout(m_Layout);
	setMaximumHeight(1000);

	connect(m_TitleInput, SIGNAL(textChanged(const QString &)), this, SLOT(UpdateBeatmapResults(const QString &)));
	connect(m_ArtistInput, SIGNAL(textChanged(const QString &)), this, SLOT(UpdateBeatmapResults(const QString &)));
	connect(m_DiffNameInput, SIGNAL(textChanged(const QString &)), this, SLOT(UpdateBeatmapResults(const QString &)));
	connect(m_MapperInput, SIGNAL(textChanged(const QString &)), this, SLOT(UpdateBeatmapResults(const QString &)));

	connect(m_BeatmapResults, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(ItemChanged(QListWidgetItem*, QListWidgetItem*)));

	UpdateBeatmapResults("");
}

std::string BeatmapSelectDialog::GetCommand()
{
	std::string command;

	if (m_BeatmapResults->currentItem() != nullptr)
	{
		if (std::stoi(((BeatmapListWidgetItem*)(m_BeatmapResults->currentItem()))->GetBeatmap().MapID) < 1)
			command += " -md5=" + ((BeatmapListWidgetItem*)(m_BeatmapResults->currentItem()))->GetBeatmap().md5;
		else
			command += " -id=" + ((BeatmapListWidgetItem*)(m_BeatmapResults->currentItem()))->GetBeatmap().MapID;
	}

	return command;
}

void BeatmapSelectDialog::UpdateBeatmapResults(const QString& string)
{
	Beatmap search;
	search.SongTitle = m_TitleInput->text().toStdString();
	search.Artist = m_ArtistInput->text().toStdString();
	search.DiffName = m_DiffNameInput->text().toStdString();
	search.Mapper = m_MapperInput->text().toStdString();

	auto results = BeatmapManager::Search(search);

	m_BeatmapResults->clear();

	for (const Beatmap* beatmap: results)
	{
		m_BeatmapResults->addItem(new BeatmapListWidgetItem(*beatmap));
	}

	//m_BeatmapResults->setCurrentRow(0);
}

void BeatmapSelectDialog::ReloadBeatmapsFromDisk()
{
	std::string command = DANSER;
	command += " -md5 0";

	system(command.c_str());

	BeatmapManager::ReloadBeatmaps();

	UpdateBeatmapResults("");
}

const Beatmap& BeatmapSelectDialog::GetCurrentBeatmap()
{
	return ((BeatmapListWidgetItem*)(m_BeatmapResults->currentItem()))->GetBeatmap();
}

void BeatmapSelectDialog::ItemChanged(QListWidgetItem* current, QListWidgetItem* previous)
{
	const Beatmap& map = ((BeatmapListWidgetItem*)(m_BeatmapResults->currentItem()))->GetBeatmap();

	if (map.BackgroundImageRelativePath.empty())
		return;

	QString fileName = Settings::CurrentSettings.General.OsuSongsDir;
	fileName += map.Directory.c_str();
	fileName += "/";
	fileName += map.BackgroundImageRelativePath.c_str();

	//std::cout << fileName.toStdString() << std::endl;


	//m_SearchGroupBox->setStyleSheet("background-image: url(" + fileName + ")");

	/*
	QPixmap backgroundImage(fileName);
	QPalette palette;
	palette.setBrush(QPalette::Window, backgroundImage);
	this->setPalette(palette);
	 */

	m_Image = QPixmap(fileName);
	m_Image = m_Image.scaled(m_GroupBox->width() - 20, m_GroupBox->height() - 20, Qt::KeepAspectRatio);
	m_ImagePreview->setPixmap(m_Image);
}