#pragma once

#include <QDialog>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QGroupBox>

#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QScrollArea>

#include "beatmap/Beatmap.h"

class BeatmapSelectDialog : public QDialog
{

	Q_OBJECT

public:
	BeatmapSelectDialog(QWidget* parent);

	std::string GetCommand();
	const Beatmap& GetCurrentBeatmap();

	// returns false if no beatmap is selected
	inline bool IsBeatmapSelected() { return m_BeatmapResults->currentItem() != nullptr; }

private slots:
	void UpdateBeatmapResults(const QString& string);
	void ReloadBeatmapsFromDisk();

	void ItemChanged(QListWidgetItem* current, QListWidgetItem* previous);

private:
	QVBoxLayout* m_Layout;

	QGridLayout* m_GroupLayout;
	QGroupBox* m_GroupBox;

	QGroupBox* m_SearchGroupBox;
	QGridLayout* m_SearchLayout;
	QLabel* m_TitleLabel;
	QLabel* m_ArtistLabel;
	QLabel* m_DiffnameLabel;
	QLabel* m_MapperLabel;

	QLineEdit* m_TitleInput;
	QLineEdit* m_ArtistInput;
	QLineEdit* m_DiffNameInput;
	QLineEdit* m_MapperInput;

	QListWidget* m_BeatmapResults;
	QPushButton* m_ReloadBeatmapsFromDisk;

	QPushButton* m_SelectButton;

	QLabel* m_ImagePreview;
	QPixmap m_Image;
};
