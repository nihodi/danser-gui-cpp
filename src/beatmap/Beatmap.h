#pragma once

#include <string>
#include <chrono>

struct Beatmap
{
	Beatmap toLowerCase() const;
	std::string toString() const;

	// useful things
	std::string Artist;
	std::string ArtistUnicode;
	std::string SongTitle;
	std::string SongTitleUnicode;
	std::string DiffName;
	std::string Mapper;

	// not so useful things
	std::string Directory;
	std::string BackgroundImageRelativePath;
	std::string FileName;
	std::string Source;
	std::string Tags;
	std::string MapID;
	std::string md5;
	std::string Length;
};
