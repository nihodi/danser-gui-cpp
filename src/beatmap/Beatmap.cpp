#include "Beatmap.h"

#include <algorithm>
#include <iostream>

// stolen from StackOverflow
std::string lower( std::string str)
{
	std::transform(str.begin(), str.end(), str.begin(),
				   [](unsigned char c){ return std::tolower(c); });
	return str;
}

Beatmap Beatmap::toLowerCase() const
{
	Beatmap newMap;

	newMap.Artist = lower(Artist);
	newMap.ArtistUnicode = lower(ArtistUnicode);
	newMap.SongTitle = lower(SongTitle);
	newMap.SongTitleUnicode = lower(SongTitleUnicode);
	newMap.DiffName = lower(DiffName);
	newMap.Mapper = lower(Mapper);

	newMap.Directory = lower(Directory);
	newMap.FileName = lower(FileName);
	newMap.Source = lower(Source);
	newMap.Tags = lower(Tags);
	newMap.MapID = lower(MapID);
	newMap.Length = lower(Length);

	return newMap;
}

std::string Beatmap::toString() const
{
	std::string text;
	if (!Source.empty())
		text += Source + " (" + Artist + ")";
	else
		text += Artist;

	text += " - " + SongTitle + " [" + DiffName + "] mapped by " + Mapper;

	return text;
}
