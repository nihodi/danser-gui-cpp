#pragma once

#include <QListWidgetItem>

#include "Beatmap.h"

class BeatmapListWidgetItem : public QListWidgetItem
{
public:
	BeatmapListWidgetItem(const Beatmap& map);

	const Beatmap& GetBeatmap() { return m_Beatmap; }

private:
	const Beatmap& m_Beatmap;

};
