#include "BeatmapListWidgetItem.h"

BeatmapListWidgetItem::BeatmapListWidgetItem(const Beatmap& map)
	: m_Beatmap(map)
{
	setText(map.toString().c_str());
}
