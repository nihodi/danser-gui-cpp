#pragma once

#include <QMainWindow>
#include <QTabWidget>
#include <QScrollArea>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow();

private:
	QTabWidget* m_TabWidget;
	QScrollArea* m_ScrollArea;

};
