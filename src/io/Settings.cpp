#include "Settings.h"

#include <QJsonDocument>

Settings Settings::CurrentSettings = Settings();

void Settings::ReadJSON(const QJsonObject& json)
{

	LoadGeneral(json["General"].toObject());
	LoadGraphics(json["Graphics"].toObject());
	LoadAudio(json["Audio"].toObject());
	LoadPlayfield(json["Playfield"].toObject());
	LoadCursor(json["Cursor"].toObject());
	LoadSkin(json["Skin"].toObject());
	LoadKnockout(json["Knockout"].toObject());
}

void Settings::WriteJSON(QJsonObject& json) const
{
	QJsonObject general;
	WriteGeneral(general);

	QJsonObject graphics;
	WriteGraphics(graphics);

	QJsonObject audio;
	WriteAudio(audio);

	QJsonObject playfield;
	WritePlayfield(playfield);

	QJsonObject cursor;
	WriteCursor(cursor);

	QJsonObject skin;
	WriteSkin(skin);

	QJsonObject knockout;
	WriteKnockout(knockout);

	json["General"] = general;
	json["Graphics"] = graphics;
	json["Audio"] = audio;
	json["Playfield"] = playfield;
	json["Cursor"] = cursor;
	json["Skin"] = skin;
	json["Knockout"] = knockout;
}

void Settings::LoadGeneral(const QJsonObject& json)
{
	General.OsuSongsDir = json["OsuSongsDir"].toString();
	General.OsuSkinsDir = json["OsuSkinsDir"].toString();

	General.DiscordPresenceOn = json["DiscordPresenceOn"].toBool(true);
	General.UnpackOszFiles = json["UnpackOszFiles"].toBool(true);
	General.VerboseImportLogs = json["VerboseImportLogs"].toBool(false);
}

void Settings::LoadGraphics(const QJsonObject& json)
{
	Graphics.Width = json["Width"].toInt(1920);
	Graphics.Height = json["Height"].toInt(1080);
	Graphics.WindowWidth = json["WindowWidth"].toInt(1440);
	Graphics.WindowHeight = json["WindowHeight"].toInt(810);

	Graphics.Fullscreen = json["Fullscreen"].toBool(true);
	Graphics.VSync = json["VSync"].toBool(false);
	Graphics.FPSCap = json["FPSCap"].toInt();
	Graphics.MSAA = json["MSAA"].toInt();
	Graphics.ShowFPS = json["ShowFPS"].toBool(true);

	Graphics.UsePersistentBuffers = json["Experimental"]["UsePersistentBuffers"].toBool();
}

void Settings::LoadAudio(const QJsonObject& json)
{
	Audio.GeneralVolume = json["GeneralVolume"].toDouble(0.5);
	Audio.MusicVolume = json["MusicVolume"].toDouble(0.5);
	Audio.SampleVolume = json["SampleVolume"].toDouble(0.5);

	Audio.Offset = json["Offset"].toInt();
	Audio.HitsoundPositionMultiplier = json["HitsoundPositionMultiplier"].toDouble(1.0);

	Audio.IgnoreBeatmapSamples = json["IgnoreBeatmapSamples"].toBool();
	Audio.IgnoreBeatmapSampleVolume = json["IgnoreBeatmapSampleVolume"].toBool();
	Audio.PlayNightcoreSamples = json["PlayNightcoreSamples"].toBool(true);
	Audio.BeatScale = json["BeatScale"].toDouble(1.2);
	Audio.BeatUseTimingPoints = json["BeatUseTimingPoints"].toBool();

	Audio.BassPlaybackBufferLength = json["Linux/Unix"]["BassPlaybackBufferLength"].toInt(100);
	Audio.BassDeviceBufferLength = json["Linux/Unix"]["BassDeviceBufferLength"].toInt(10);
	Audio.BassPlaybackBufferLength = json["Linux/Unix"]["BassUpdatePeriod"].toInt(5);
	Audio.BassPlaybackBufferLength = json["Linux/Unix"]["BassDeviceUpdatePeriod"].toInt(10);
}

void Settings::LoadPlayfield(const QJsonObject& json)
{
	// General
	Playfield.DrawObjects = json["DrawObjects"].toBool(true);
	Playfield.DrawCursors = json["DrawCursors"].toBool(true);
	Playfield.Scale = json["Scale"].toDouble(1.0);
	Playfield.OsuShift = json["OsuShift"].toBool(false);
	Playfield.ShiftX = json["ShiftX"].toDouble(0.0);
	Playfield.ShiftY = json["ShiftY"].toDouble(0.0);
	Playfield.ScaleStoryboardWithPlayfield = json["ScaleStoryboardWithPlayfield"].toBool(false);
	Playfield.LeadInTime = json["LeadInTime"].toDouble(5.0);
	Playfield.LeadInHold = json["LeadInHold"].toDouble(2.0);
	Playfield.FadeOutTime = json["FadeOutTime"].toDouble(5.0);

	// seizure warning
	Playfield.SeizureWarning.Enabled = json["SeizureWarning"]["Enabled"].toBool(true);
	Playfield.SeizureWarning.Duration = json["SeizureWarning"]["Duration"].toDouble(5.0);

	// background
	Playfield.Background.LoadStoryboards = json["Background"]["LoadStoryboards"].toBool(true);
	Playfield.Background.LoadVideos = json["Background"]["LoadVideos"].toBool(false);
	Playfield.Background.FlashToTheBeat = json["Background"]["FlashToTheBeat"].toBool(false);

	// background dim
	Playfield.Background.Dim.Intro = json["Background"]["Dim"]["Intro"].toDouble(0.0);
	Playfield.Background.Dim.Normal = json["Background"]["Dim"]["Normal"].toDouble(0.95);
	Playfield.Background.Dim.Breaks = json["Background"]["Dim"]["Breaks"].toDouble(0.5);

	// background parallax
	Playfield.Background.Parallax.Amount = json["Background"]["Parallax"]["Amount"].toDouble(0.1);
	Playfield.Background.Parallax.Speed = json["Background"]["Parallax"]["Speed"].toDouble(0.5);

	// background blur
	Playfield.Background.Blur.Enabled = json["Background"]["Blur"]["Enabled"].toBool(false);
	// values
	Playfield.Background.Blur.Values.Intro = json["Background"]["Blur"]["Values"]["Intro"].toDouble(0.0);
	Playfield.Background.Blur.Values.Normal = json["Background"]["Blur"]["Values"]["Normal"].toDouble(0.6);
	Playfield.Background.Blur.Values.Breaks = json["Background"]["Blur"]["Values"]["Breaks"].toDouble(0.3);

	// triangles
	Playfield.Background.Triangles.Enabled = json["Background"]["Triangles"]["Enabled"].toBool(false);
	Playfield.Background.Triangles.Shadowed = json["Background"]["Triangles"]["Shadowed"].toBool(true);
	Playfield.Background.Triangles.DrawOverBlur = json["Background"]["Triangles"]["DrawOverBlur"].toBool(true);

	Playfield.Background.Triangles.ParallaxMultiplier = json["Background"]["Triangles"]["ParallaxMultiplier"].toDouble(0.5);
	Playfield.Background.Triangles.Density = json["Background"]["Triangles"]["Density"].toDouble(1.0);
	Playfield.Background.Triangles.Scale = json["Background"]["Triangles"]["Scale"].toDouble(1.0);
	Playfield.Background.Triangles.Speed = json["Background"]["Triangles"]["Speed"].toDouble(1.0);

	// Logo
	Playfield.Logo.DrawSpectrum = json["Logo"]["DrawSpectrum"].toBool(false);
	Playfield.Logo.Dim.Intro = json["Logo"]["Dim"]["Intro"].toDouble(0.0);
	Playfield.Logo.Dim.Normal = json["Logo"]["Dim"]["Normal"].toDouble(1.0);
	Playfield.Logo.Dim.Breaks = json["Logo"]["Dim"]["Breaks"].toDouble(1.0);

	// Bloom
	Playfield.Bloom.Enabled = json["Bloom"]["Enabled"].toBool(false);
	Playfield.Bloom.BloomToTheBeat = json["Bloom"]["BloomToTheBeat"].toBool(true);
	Playfield.Bloom.BloomBeatAddition = json["Bloom"]["BloomBeatAddition"].toDouble(0.3);
	Playfield.Bloom.Threshold = json["Bloom"]["Threshold"].toDouble(0.0);
	Playfield.Bloom.Blur = json["Bloom"]["Blur"].toDouble(0.6);
	Playfield.Bloom.Power = json["Bloom"]["Power"].toDouble(0.7);

}

void Settings::LoadCursor(const QJsonObject& json)
{
	Cursor.TrailStyle = json["TrailStyle"].toInt(1);
	Cursor.Style23Speed = json["Style23Speed"].toDouble(0.18);
	Cursor.Style4Shift = json["Style4Shift"].toDouble(0.5);

	// colors
	Cursor.Colors.EnableRainbow = json["Colors"]["EnableRainbow"].toBool(true);
	Cursor.Colors.RainbowSpeed = json["Colors"]["RainbowSpeed"].toDouble(8.0);

	Cursor.Colors.BaseColor.Hue = json["Colors"]["BaseColor"]["Hue"].toInt(0);
	Cursor.Colors.BaseColor.Saturation = json["Colors"]["BaseColor"]["Saturation"].toInt(1);
	Cursor.Colors.BaseColor.Value = json["Colors"]["BaseColor"]["Value"].toInt(1);

	Cursor.Colors.EnableCustomHueOffset = json["Colors"]["EnableCustomHueOffset"].toBool(false);
	Cursor.Colors.HueOffset = json["Colors"]["HueOffset"].toDouble(0);
	Cursor.Colors.FlashToTheBeat = json["Colors"]["FlashToTheBeat"].toBool(false);
	Cursor.Colors.FlashAmplitude = json["Colors"]["HueOffset"].toDouble(0);

	Cursor.EnableCustomTagColorOffset = json["EnableCustomTagColorOffset"].toBool(true);
	Cursor.TagColorOffset = json["TagColorOffset"].toDouble(-36);

	//trail things
	Cursor.EnableTrailGlow = json["EnableTrailGlow"].toBool(true);
	Cursor.EnableCustomTrailGlowOffset = json["EnableCustomTrailGlowOffset"].toBool(true);
	Cursor.TrailGlowOffset = json["EnableCustomTrailGlowOffset"].toInt(-36);
	Cursor.TrailScale = json["TrailScale"].toDouble(1.0);
	Cursor.TrailEndScale = json["TrailEndScale"].toDouble(0.4);
	Cursor.TrailDensity = json["TrailDensity"].toDouble(1.0);
	Cursor.TrailMaxLength = json["TrailMaxLength"].toInt(2000);
	Cursor.TrailRemoveSpeed = json["TrailRemoveSpeed"].toDouble(1.0);
	Cursor.GlowEndScale = json["GlowEndScale"].toDouble(0.4);
	Cursor.InnerLengthMult = json["InnerLengthMult"].toDouble(0.9);
	Cursor.AdditiveBlending = json["AdditiveBlending"].toBool(true);
	Cursor.CursorRipples = json["CursorRipples"].toBool(true);
	Cursor.SmokeEnabled = json["SmokeEnabled"].toBool(true);

	Cursor.CursorSize = json["CursorSize"].toInt(12);
	Cursor.CursorExpand = json["CursorExpand"].toBool(false);
	Cursor.ScaleToTheBeat = json["ScaleToTheBeat"].toBool(false);
	Cursor.ShowCursorsOnBreaks = json["ShowCursorsOnBreaks"].toBool(true);
	Cursor.BounceOnEdges = json["BounceOnEdges"].toBool(false);

}

void Settings::LoadSkin(const QJsonObject& json)
{
	Skin.CurrentSkin = json["CurrentSkin"].toString("default");
	Skin.FallbackSkin = json["FallbackSkin"].toString("default");
	Skin.UseColorsFromSkin = json["UseColorsFromSkin"].toBool(false);
	Skin.UseBeatmapColors = json["UseBeatmapColors"].toBool(false);

	Skin.Cursor.UseSkinCursor = json["Cursor"]["UseSkinCursor"].toBool(false);
	Skin.Cursor.Scale = json["Cursor"]["Scale"].toDouble(1.0);
	Skin.Cursor.TrailScale = json["Cursor"]["TrailScale"].toDouble(1.0);
	Skin.Cursor.ForceLongTrail = json["Cursor"]["ForceLongTrail"].toBool(false);
	Skin.Cursor.LongTrailLength = json["Cursor"]["LongTrailLength"].toInt(2048);
	Skin.Cursor.LongTrailDensity = json["Cursor"]["LongTrailDensity"].toDouble(1);
}

void Settings::LoadKnockout(const QJsonObject& json)
{

	Knockout.Mode = json["Mode"].toInt(0);
	Knockout.GraceEndTime = json["GraceEndTime"].toDouble(-10);
	Knockout.BubbleMinimumCombo = json["Mode"].toInt(200);
	Knockout.ExcludeMods = json["ExcludeMods"].toString("");
	Knockout.HideMods = json["HideMods"].toString("");
	Knockout.MaxPlayers = json["MaxPlayers"].toInt(50);
	Knockout.RevivePlayersAtEnd = json["Mode"].toBool(false);
	Knockout.LiveSort = json["LiveSort"].toBool(true);
	Knockout.SortBy = json["Mode"].toString("Score");
	Knockout.HideOverlayOnBreaks = json["HideOverlayOnBreaks"].toBool(false);
	Knockout.MinCursorSize = json["MinCursorSize"].toDouble(3);
	Knockout.MaxCursorSize = json["MaxCursorSize"].toDouble(7);
	Knockout.AddDanser = json["AddDanser"].toBool(false);
	Knockout.DanserName = json["DanserName"].toString("danser");
}

void Settings::WriteGeneral(QJsonObject& json) const
{
	json["OsuSongsDir"] = General.OsuSongsDir;
	json["OsuSkinsDir"] = General.OsuSkinsDir;

	json["DiscordPresenceOn"] = General.DiscordPresenceOn;
	json["UnpackOszFiles"] = General.UnpackOszFiles;
	json["VerboseImportLogs"] = General.VerboseImportLogs;
}

void Settings::WriteGraphics(QJsonObject& json) const
{
	json["Width"] = Graphics.Width;
	json["Height"] = Graphics.Height;
	json["WindowWidth"] = Graphics.WindowWidth;
	json["WindowHeight"] = Graphics.WindowHeight;

	json["Fullscreen"] = Graphics.Fullscreen;
	json["VSync"] = Graphics.VSync;
	json["FPSCap"] = Graphics.FPSCap;
	json["MSAA"] = Graphics.MSAA;
	json["ShowFPS"] = Graphics.ShowFPS;

	QJsonObject experimental = json["Experimental"].toObject();
	experimental["UsePersistentBuffers"] = Graphics.UsePersistentBuffers;

	json["Experimental"] = experimental;
}


void Settings::WriteAudio(QJsonObject& json) const
{
	json["GeneralVolume"] = Audio.GeneralVolume;
	json["MusicVolume"] = Audio.MusicVolume;
	json["SampleVolume"] = Audio.SampleVolume;

	json["Offset"] = Audio.Offset;
	json["HitsoundPositionMultiplier"] = Audio.HitsoundPositionMultiplier;

	json["IgnoreBeatmapSamples"] = Audio.IgnoreBeatmapSamples;
	json["IgnoreBeatmapSampleVolume"] = Audio.IgnoreBeatmapSampleVolume;
	json["BeatScale"] = Audio.BeatScale;
	json["BeatUseTimingPoints"] = Audio.BeatUseTimingPoints;

	QJsonObject linux_unix;
	linux_unix["BassPlaybackBufferLength"] = Audio.BassPlaybackBufferLength;
	linux_unix["BassDeviceBufferLength"] = Audio.BassDeviceBufferLength;
	linux_unix["BassUpdatePeriod"] = Audio.BassUpdatePeriod;
	linux_unix["BassDeviceUpdatePeriod"] = Audio.BassDeviceUpdatePeriod;
}


void Settings::WritePlayfield(QJsonObject& json) const
{
	// General
	json["DrawObjects"] = Playfield.DrawObjects;
	json["DrawCursors"] = Playfield.DrawCursors;
	json["Scale"] = Playfield.Scale;
	json["OsuShift"] = Playfield.OsuShift;
	json["ShiftX"] = Playfield.ShiftX;
	json["ShiftY"] = Playfield.ShiftY;
	json["ScaleStoryboardWithPlayfield"] = Playfield.ScaleStoryboardWithPlayfield;
	json["LeadInTime"] = Playfield.LeadInTime;
	json["LeadInHold"] = Playfield.LeadInHold;
	json["FadeOutTime"] = Playfield.FadeOutTime;

	// seizure warning
		QJsonObject seizureWarning;
		seizureWarning["Enabled"] = Playfield.SeizureWarning.Enabled;
		seizureWarning["Duration"] = Playfield.SeizureWarning.Duration;

		// background
		QJsonObject background;
		background["LoadStoryboards"] = Playfield.Background.LoadStoryboards;
		background["LoadVideos"] = Playfield.Background.LoadVideos;
		background["FlashToTheBeat"] = Playfield.Background.FlashToTheBeat;

			// background dim
			QJsonObject backgroundDim;
			backgroundDim["Intro"] = Playfield.Background.Dim.Intro;
			backgroundDim["Normal"] = Playfield.Background.Dim.Normal;
			backgroundDim["Breaks"] = Playfield.Background.Dim.Breaks;

			// background parallax
			QJsonObject backgroundParallax;
			backgroundParallax["Amount"] = Playfield.Background.Parallax.Amount;
			backgroundParallax["Speed"] = Playfield.Background.Parallax.Speed;

			// background blur
			QJsonObject backgroundBlur;
			backgroundBlur["Enabled"] = Playfield.Background.Blur.Enabled;

				// blur values
				QJsonObject blurValues;
				blurValues["Intro"] = Playfield.Background.Blur.Values.Intro;
				blurValues["Normal"] = Playfield.Background.Blur.Values.Normal;
				blurValues["Breaks"] = Playfield.Background.Blur.Values.Breaks;

			backgroundBlur["Values"] = blurValues;

			// background triangles
			QJsonObject backgroundTriangles;
			backgroundTriangles["Enabled"] = Playfield.Background.Triangles.Enabled;
			backgroundTriangles["Shadowed"] = Playfield.Background.Triangles.Shadowed;
			backgroundTriangles["DrawOverBlur"] = Playfield.Background.Triangles.DrawOverBlur;

			backgroundTriangles["ParallaxMultiplier"] = Playfield.Background.Triangles.ParallaxMultiplier;
			backgroundTriangles["Density"] = Playfield.Background.Triangles.Density;
			backgroundTriangles["Scale"] = Playfield.Background.Triangles.Scale;
			backgroundTriangles["Speed"] = Playfield.Background.Triangles.Speed;

	background["Dim"] = backgroundDim;
	background["Parallax"] = backgroundParallax;
	background["Blur"] = backgroundBlur;
	background["Triangles"] = backgroundTriangles;

	// Logo
	QJsonObject logo;
	logo["DrawSpectrum"] = Playfield.Logo.DrawSpectrum;

		// log dim
		QJsonObject logoDim;
		logoDim["Intro"] = Playfield.Logo.Dim.Intro;
		logoDim["Normal"] = Playfield.Logo.Dim.Normal;
		logoDim["Breaks"] = Playfield.Logo.Dim.Breaks;

	// Bloom
	QJsonObject bloom;
	bloom["Enabled"] = Playfield.Bloom.Enabled;
	bloom["BloomToTheBeat"] = Playfield.Bloom.BloomToTheBeat;
	bloom["BloomBeatAddition"] = Playfield.Bloom.BloomBeatAddition;
	bloom["Threshold"] = Playfield.Bloom.Threshold;
	bloom["Blur"] = Playfield.Bloom.Blur;
	bloom["Power"] = Playfield.Bloom.Power;

	logo["Dim"] = logoDim;

	json["SeizureWarning"] = seizureWarning;
	json["Background"] = background;
	json["Logo"] = logo;
	json["Bloom"] = bloom;
}

void Settings::WriteCursor(QJsonObject& json) const
{
	json["TrailStyle"] = Cursor.TrailStyle;
	json["Style23Speed"] = Cursor.Style23Speed;
	json["Style4Shift"] = Cursor.Style4Shift;

		// colors
		QJsonObject colors;
		colors["EnableRainbow"] = Cursor.Colors.EnableRainbow;
		colors["RainbowSpeed"] = Cursor.Colors.RainbowSpeed;

			// cursor base color
			QJsonObject cursorBaseColor;
			cursorBaseColor["Hue"] = Cursor.Colors.BaseColor.Hue;
			cursorBaseColor["Saturation"] = Cursor.Colors.BaseColor.Saturation;
			cursorBaseColor["Value"] = Cursor.Colors.BaseColor.Value;

			colors["BaseColor"] = cursorBaseColor;

		colors["EnableCustomHueOffset"] = Cursor.Colors.EnableCustomHueOffset;
		colors["HueOffset"] = Cursor.Colors.HueOffset;
		colors["FlashToTheBeat"] = Cursor.Colors.FlashToTheBeat;
		colors["FlashAmplitude"] = Cursor.Colors.FlashAmplitude;

	json["EnableCustomTagColorOffset"] = Cursor.EnableCustomTagColorOffset;
	json["TagColorOffset"] = Cursor.TagColorOffset;

	//trail things
	json["EnableTrailGlow"] = Cursor.EnableTrailGlow;
	json["EnableCustomTrailGlowOffset"] = Cursor.EnableCustomTrailGlowOffset;
	json["TrailGlowOffset"] = Cursor.TrailGlowOffset;
	json["TrailScale"] = Cursor.TrailScale;
	json["TrailEndScale"] = Cursor.TrailEndScale;
	json["TrailDensity"] = Cursor.TrailDensity;
	json["TrailMaxLength"] = Cursor.TrailMaxLength;
	json["TrailRemoveSpeed"] = Cursor.TrailRemoveSpeed;
	json["GlowEndScale"] = Cursor.GlowEndScale;
	json["InnerLengthMult"] = Cursor.InnerLengthMult;
	json["AdditiveBlending"] = Cursor.AdditiveBlending;
	json["CursorRipples"] = Cursor.CursorRipples;
	json["SmokeEnabled"] = Cursor.SmokeEnabled;
	json["EnableTrailGlow"] = Cursor.EnableTrailGlow;

	json["CursorSize"] = Cursor.CursorSize;
	json["CursorExpand"] = Cursor.CursorExpand;
	json["ScaleToTheBeat"] = Cursor.ScaleToTheBeat;
	json["ShowCursorsOnBreaks"] = Cursor.ShowCursorsOnBreaks;
	json["BounceOnEdges"] = Cursor.BounceOnEdges;

	json["Colors"] = colors;
}


void Settings::WriteSkin(QJsonObject& json) const
{
	json["CurrentSkin"] = Skin.CurrentSkin;
	json["FallbackSkin"] = Skin.FallbackSkin;
	json["UseColorsFromSkin"] = Skin.UseColorsFromSkin;
	json["UseBeatmapColors"] = Skin.UseBeatmapColors;

		// cursor
		QJsonObject cursor;
		cursor["UseSkinCursor"] = Skin.Cursor.UseSkinCursor;
		cursor["Scale"] = Skin.Cursor.Scale;
		cursor["TrailScale"] = Skin.Cursor.TrailScale;
		cursor["ForceLongTrail"] = Skin.Cursor.ForceLongTrail;
		cursor["LongTrailLength"] = Skin.Cursor.LongTrailLength;
		cursor["LongTrailDensity"] = Skin.Cursor.LongTrailDensity;

	json["Cursor"] = cursor;
}

void Settings::WriteKnockout(QJsonObject& json) const
{
	json["Mode"] = Knockout.Mode;
	json["GraceEndTime"] = Knockout.GraceEndTime;
	json["BubbleMinimumCombo"] = Knockout.BubbleMinimumCombo;
	json["ExcludeMods"] = Knockout.ExcludeMods;
	json["HideMods"] = Knockout.HideMods;
	json["MaxPlayers"] = Knockout.MaxPlayers;
	json["RevivePlayersAtEnd"] = Knockout.RevivePlayersAtEnd;
	json["LiveSort"] = Knockout.LiveSort;
	json["SortBy"] = Knockout.SortBy;
	json["HideOverlayOnBreaks"] = Knockout.HideOverlayOnBreaks;
	json["MinCursorSize"] = Knockout.MinCursorSize;
	json["MaxCursorSize"] = Knockout.MaxCursorSize;
	json["AddDanser"] = Knockout.AddDanser;
	json["DanserName"] = Knockout.DanserName;
}
