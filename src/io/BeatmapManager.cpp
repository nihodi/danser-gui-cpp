#include "BeatmapManager.h"
#include "platform.h"

#include <sqlite3.h>
#include <iostream>

std::vector<Beatmap> BeatmapManager::s_Beatmaps;

void BeatmapManager::Init()
{
	std::cout << "Loading beatmaps from danser.db ..." << std::endl;

	sqlite3* db;

#ifdef linux

	std::string location = HOME;
#else
    std::string location;
#endif

    location += DB_LOCATION;

	std::string sqlCommand = "SELECT * FROM beatmaps";

	int failure = sqlite3_open(location.c_str(), &db);
	if (failure)
	{
		// try again with relative path if on linux
#ifdef linux
		location = "danser.db";
		failure = sqlite3_open(location.c_str(), &db);

		if (failure)
		{

			std::cout << "could not open database ";
			std::cout << location << std::endl;
			return;
#endif

#ifdef linux
		}
#endif
	}

	sqlite3_exec(db, sqlCommand.c_str(), BeatmapManager::db_callback, nullptr, nullptr);

	/*
	for (const Beatmap& beatmap : s_Beatmaps)
	{
		std::cout << "Beatmap: " << std::endl;
		std::cout << "Title: " << beatmap.SongTitle << " Artist: " << beatmap.Artist << std::endl;
	}
	 */

	sqlite3_close(db);

	std::cout << "Finished loading beatmaps from danser.db!" << std::endl;

}

int BeatmapManager::db_callback(void* NotUsed, int argc, char** argv, char** azColName)
{
	/* DB LAYOUT
	 * =========
	 * 0: Dir
	 * 1: Filename
	 * 2: LastModified
	 * 3: Title
	 * 4. TitleUnicode
	 * 5. Artist
	 * 6. ArtistUnicode
	 * 7. Creator (Mapper)
	 * 8. Version (Diffname)
	 * 9. Source
	 * 10: Tags
	 * 11: CS
	 * 12: AR
	 * 13: SliderMultiplier
	 * 14: SliderTickRate
	 * 15: AudioFile
	 * 16: PreviewTime
	 * 17: SampleSet
	 * 18: StackLeniency
	 * 19: Mode
	 * 20: BG
	 * 21: md5 sum
	 * 22: DateAdded
	 * 23: PlayCount
	 * 24: LastPlayed
	 * 25: HPDrain
	 * 26: OD
	 * 27: Stars (doesn't work)
	 * 28: BPM (minimum)
	 * 29: BPM (maximum)
	 * 30: Circles
	 * 31: Sliders
	 * 32: Spinners
	 * 33: Endtime (length in milliseconds)
	 * 34: MapsetID
	 * 35: MapID
	 */

	// mode '0' is osu!standard
	// filter out beatmaps that are not osu!standard
	char* mode = argv[19];
	if (mode[0] == '0')
	{
		Beatmap map;
		map.Directory = argv[0];
		map.BackgroundImageRelativePath = argv[20];
		map.FileName = argv[1];
		map.SongTitle = argv[3];
		map.SongTitleUnicode = argv[4];
		map.Artist = argv[5];
		map.ArtistUnicode = argv[6];
		map.Mapper = argv[7];
		map.DiffName = argv[8];
		map.Source = argv[9];
		map.Tags = argv[10];
		map.MapID = argv[35];
		map.Length = argv[33];
		map.md5 = argv[21];

		s_Beatmaps.push_back(map);
	}

	// debug
	/*
	for (i = 0; i < argc; i++)
	{
		if (argv[i])
			std::cout << azColName[i] << " = " << argv[i] << std::endl;
		else
			std::cout << azColName[i] << " = NULL" << std::endl;
	}*/
	return 0;
}

std::vector<const Beatmap*> BeatmapManager::Search(SearchTerm search)
{
	search = search.toLowerCase();

	std::vector<const Beatmap*> result;

	for (const Beatmap& map : s_Beatmaps)
	{
		// string.find_first_of(x) != std::string::npos
		// is the same as string.contains

		Beatmap searchMap = map.toLowerCase();

		// check artist
		if ((searchMap.Artist.find(search.Artist) != std::string::npos) || search.Artist.empty())
			// check song title
			if ((searchMap.SongTitle.find(search.SongTitle) != std::string::npos) || search.SongTitle.empty())
				// check diffname
				if ((searchMap.DiffName.find(search.DiffName) != std::string::npos) || search.DiffName.empty())
					// check mapper
					if ((searchMap.Mapper.find(search.Mapper) != std::string::npos) || search.Mapper.empty())
						result.push_back(&map);

	}

	return result;

}

void BeatmapManager::ReloadBeatmaps()
{
	s_Beatmaps.clear();
	Init();
}
