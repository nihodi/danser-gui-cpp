#pragma once

#include <QJsonObject>

class Settings
{
public:
	Settings() = default;

	void ReadJSON(const QJsonObject& json);
	void WriteJSON(QJsonObject& json) const;

private:
	void LoadGeneral(const QJsonObject& json);
	void LoadGraphics(const QJsonObject& json);
	void LoadAudio(const QJsonObject& json);
	void LoadPlayfield(const QJsonObject& json);
	void LoadCursor(const QJsonObject& json);
	void LoadSkin(const QJsonObject& json);
	void LoadKnockout(const QJsonObject& json);
	void WriteGeneral(QJsonObject& json) const;
	void WriteGraphics(QJsonObject& json) const;
	void WriteAudio(QJsonObject& json) const;
	void WritePlayfield(QJsonObject& json) const;
	void WriteCursor(QJsonObject& json) const;
	void WriteSkin(QJsonObject& json) const;
	void WriteKnockout(QJsonObject& json) const;

	// Many structs representing settings

	struct DimSettings
	{
		// range 0-1
		double Intro, Normal, Breaks;
	};

	struct ParallaxSettings
	{
		// range 0-1
		double Amount, Speed;
	};

	struct BlurSettings
	{
		bool Enabled;
		DimSettings Values; // it's called Values in the JSON so might as well call it that here
	};

	struct TrianglesSettings
	{
		bool Enabled, Shadowed, DrawOverBlur;
		// range >=0
		double ParallaxMultiplier, Density, Scale, Speed;
	};

	struct SeizureWarningSettings
	{
		bool Enabled;
		// range 0-1
		double Duration;
	};

	struct BackgroundSettings
	{
		bool LoadStoryboards, LoadVideos, FlashToTheBeat;
		DimSettings Dim;
		ParallaxSettings Parallax;
		BlurSettings Blur;
		TrianglesSettings Triangles;
	};

	struct LogoSettings
	{
		bool DrawSpectrum;
		DimSettings Dim;
	};

	struct BloomSettings
	{
		bool Enabled, BloomToTheBeat;
		double BloomBeatAddition, Threshold, Blur, Power;
	};

	struct GeneralSettings
	{
		QString OsuSongsDir;
		QString OsuSkinsDir;

		bool DiscordPresenceOn;
		bool UnpackOszFiles;
		bool VerboseImportLogs;
	};

	struct GraphicsSettings
	{
		// MSAA range: 0-16

		int Width, Height, WindowWidth, WindowHeight, FPSCap, MSAA;
		bool Fullscreen, VSync, ShowFPS, UsePersistentBuffers;
	};

	struct AudioSettings
	{
		// volume range: 0-1

		double GeneralVolume, MusicVolume, SampleVolume, HitsoundPositionMultiplier, BeatScale;
		int Offset;
		bool IgnoreBeatmapSamples, IgnoreBeatmapSampleVolume, PlayNightcoreSamples, BeatUseTimingPoints;

		// Linux/Unix
		int BassPlaybackBufferLength, BassDeviceBufferLength, BassUpdatePeriod, BassDeviceUpdatePeriod;
	};

	struct PlayfieldSettings
	{
		bool DrawObjects, DrawCursors, OsuShift, ScaleStoryboardWithPlayfield;
		double Scale, ShiftX, ShiftY, LeadInTime, LeadInHold, FadeOutTime;

		SeizureWarningSettings SeizureWarning;
		BackgroundSettings Background;
		LogoSettings Logo;
		BloomSettings Bloom;
	};

	struct HSVSettings
	{
		int Hue, Saturation, Value;
	};

	struct CursorColorSettings
	{
		bool EnableRainbow, EnableCustomHueOffset, FlashToTheBeat;
		double RainbowSpeed, HueOffset, FlashAmplitude;
		HSVSettings BaseColor;
	};

	struct CursorSettings
	{
		int TrailStyle, TrailGlowOffset, CursorSize, TrailMaxLength;
		double Style23Speed, Style4Shift, TagColorOffset, TrailScale, TrailEndScale, TrailDensity, TrailRemoveSpeed;
		double GlowEndScale, InnerLengthMult;
		bool EnableCustomTagColorOffset, EnableTrailGlow, EnableCustomTrailGlowOffset, CursorExpand, ScaleToTheBeat;
		bool ShowCursorsOnBreaks, BounceOnEdges, AdditiveBlending, CursorRipples, SmokeEnabled;
		CursorColorSettings Colors;
	};

	struct SkinCursorSettings
	{
		bool UseSkinCursor, ForceLongTrail;
		double Scale, TrailScale, LongTrailDensity;
		int LongTrailLength;
	};

	struct SkinSettings
	{
		QString CurrentSkin, FallbackSkin;
		bool UseColorsFromSkin, UseBeatmapColors;

		SkinCursorSettings Cursor;
	};

	struct KnockoutSettings
	{
		int Mode, BubbleMinimumCombo, MaxPlayers;
		double GraceEndTime, MinCursorSize, MaxCursorSize;
		bool RevivePlayersAtEnd, LiveSort, HideOverlayOnBreaks, AddDanser;
		QString ExcludeMods, HideMods, SortBy, DanserName;
	};

public:
	static Settings CurrentSettings;

	GeneralSettings General;
	GraphicsSettings Graphics;
	AudioSettings Audio;
	PlayfieldSettings Playfield;
	CursorSettings Cursor;
	SkinSettings Skin;
	KnockoutSettings Knockout;
};
