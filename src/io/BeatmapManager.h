#pragma once

#include <vector>
#include "beatmap/Beatmap.h"

using SearchTerm = Beatmap;

class BeatmapManager
{

public:
	BeatmapManager() = delete;

	static void Init();
	static void ReloadBeatmaps();
	static int db_callback(void* NotUsed, int argc, char** argv, char** azColName);

	static std::vector<const Beatmap*> Search(SearchTerm search);

private:
	static std::vector<Beatmap> s_Beatmaps;


};
