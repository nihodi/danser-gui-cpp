// Platform-specific defines / functions

#pragma once

// HOME should expand to the users home directory
// expands to %USERPROFILE% on Windows
#ifdef linux

	#define HOME getenv("HOME")
	#define DANSER "danser"
	#define SETTINGS_LOCATION ".config/danser/"
	#define DB_LOCATION "/.local/share/danser/danser.db"

#else

	#define HOME getenv("USERPROFILE")
	#define DANSER "danser.exe"
	#define SETTINGS_LOCATION "settings/"
	#define DB_LOCATION "danser.db"

#endif