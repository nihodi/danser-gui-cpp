#include "MainWindow.h"

#include "widgets/playback/PlaybackWidget.h"
#include "widgets/settings/SettingsWidget.h"
#include "widgets/AboutAndHelpWidget.h"

#include "dialogues/BeatmapSelectDialog.h"

MainWindow::MainWindow()
{
	m_TabWidget = new QTabWidget(this);

	m_TabWidget->addTab(new PlaybackWidget, "Playback");
	m_TabWidget->addTab(new SettingsWidget, "Settings");
	m_TabWidget->addTab(new AboutAndHelpWidget, "Help/About");

	m_ScrollArea = new QScrollArea;
	m_ScrollArea->setWidget(m_TabWidget);
	m_ScrollArea->setWidgetResizable(true);

	setCentralWidget(m_ScrollArea);
	showMaximized();
}
