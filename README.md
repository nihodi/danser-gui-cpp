# danser-gui-cpp
________________

A simple GUI for [Danser](https://github.com/wieku/danser-go/) written in C++ using Qt.

If you experience any bugs, or have any suggestions, feel free to create an [issue](https://gitlab.com/nihodi/danser-gui-cpp/-/issues)
describing your problem/suggestion.

## Installation instructions
____________________________

Go [here](repo/INSTALL_WINDOWS.md) for installation instructions on Windows.

Go [here](repo/INSTALL_LINUX.md) for installation instructions on Linux.

### Notes:

- See [usage](repo/USAGE.md) for more detailed usage.
- **If you wish to record videos, download [ffmpeg](https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-n5.0-latest-win64-gpl-5.0.zip) and put `ffmpeg.exe` inside the installation folder.**
- **THE GUI WILL LOOK DIFFERENT FROM THE SCREENSHOTS. THIS IS BECAUSE THE SCREENSHOTS HAVE BEEN TAKEN ON LINUX** 


## TODO
_______
- ~~Add support for playing replays in a separate tab~~  - Done
- ~~Add support for overriding start/end point of playback~~  - Done
- ~~Add support for selecting a beatmap using beatmap IDs~~ - Done
- ~~Make Replay tab look better using QGroupBox~~ - Done
- ~~Add support for overriding map OD/AR/HP/CS~~ - Done. Only AR and CS support was added because changing OD and HP does nothing.
- ~~Add support for editing Danser settings (resolution, skin, FPS cap and many more things).
For now this can be edited manually by editing the `settings/danser-gui-cpp.json` file.~~
In progress. Most settings are availible and more are being added.
- ~~Add tooltips to everything.~~ - Done. Added to all things that I thought were necessary.
- Add a link to the Danser wiki for in-depth explanations of settings.
- Actually release this lol

#### Preview Image
__________________
![](repo/img/showcase_readme.png)
